/*
SQLyog Ultimate v12.5.0 (64 bit)
MySQL - 5.7.21 : Database - shoppingdb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`shoppingdb` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `shoppingdb`;

/*Table structure for table `cart` */

DROP TABLE IF EXISTS `cart`;

CREATE TABLE `cart` (
  `id` int(11) DEFAULT NULL,
  `serial_num` varchar(32) NOT NULL,
  `trade_price` float DEFAULT NULL,
  `trade_num` int(11) DEFAULT NULL,
  `trade_name` varchar(32) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `picture` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`serial_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cart` */

/*Table structure for table `customerorder` */

DROP TABLE IF EXISTS `customerorder`;

CREATE TABLE `customerorder` (
  `ID` int(11) DEFAULT NULL,
  `serial_num` bigint(20) NOT NULL,
  `trade_name` varchar(64) DEFAULT NULL,
  `trade_price` float DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `picture` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`serial_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `customerorder` */

/*Table structure for table `form` */

DROP TABLE IF EXISTS `form`;

CREATE TABLE `form` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) DEFAULT NULL,
  `sellEmail` varchar(32) DEFAULT NULL,
  `title` varchar(32) DEFAULT NULL,
  `content` text,
  KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `form` */

/*Table structure for table `trades` */

DROP TABLE IF EXISTS `trades`;

CREATE TABLE `trades` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `serial_num` varchar(32) NOT NULL,
  `trade_num` varchar(32) DEFAULT NULL,
  `trade_name` varchar(32) DEFAULT NULL,
  `trade_carriage` float DEFAULT NULL,
  `trade_price` float DEFAULT NULL,
  `trade_brand` varchar(32) DEFAULT NULL,
  `trade_type` varchar(32) DEFAULT NULL,
  `trade_style` varchar(32) DEFAULT NULL,
  `trade_size` varchar(8) DEFAULT NULL,
  `picture` longblob,
  PRIMARY KEY (`serial_num`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `trades` */

insert  into `trades`(`ID`,`serial_num`,`trade_num`,`trade_name`,`trade_carriage`,`trade_price`,`trade_brand`,`trade_type`,`trade_style`,`trade_size`,`picture`) values 
(4,'123456_mz','5','秋衣',20,320,'艾草','男装','成熟','M',NULL),
(2,'123456_tz','15','秋裤',10,530,'巴拉巴拉','童装','成熟','S',NULL),
(5,'123456_wz','15','衬衫',10,530,'艾薇儿','女装','成熟','S',NULL),
(7,'123457_mz','15','衬衫',10,530,'海澜之家','男装','成熟','S',NULL),
(8,'123457_tz','15','秋裤',10,530,'巴拉巴拉','童装','成熟','S',NULL);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `ID` int(11) DEFAULT NULL,
  `usercode` varchar(32) NOT NULL,
  `username` varchar(32) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `usertype` int(11) DEFAULT NULL,
  `Email` varchar(32) DEFAULT NULL,
  `telephone` varchar(11) DEFAULT NULL,
  `address` varchar(64) DEFAULT NULL,
  `picture` varchar(64) DEFAULT NULL,
  `shopname` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`usercode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
