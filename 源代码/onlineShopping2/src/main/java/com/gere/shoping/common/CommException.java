package com.gere.shoping.common;

public class CommException extends RuntimeException{

	private static final long serialVersionUID = -6770425927610955212L;
	private String code;
	private String message;
	/**
	 * 默认构造函数
	 */
	public CommException() {
		super();
	}
	/**
	 * 传递消息的构造函数
	 * @param message 传递的消息
	 */
	public CommException(String message) {
		this.code = "1000";
		this.message = message;
	}
	/**
	 * 传递编码和消息的构造函数
	 * @param code
	 * @param message
	 */
	public CommException(String code, String message) {
		this.code = code;
		this.message = message;
	}
	
	public String getCode() {
		return code;
	}
	public String getMessage() {
		return message;
	}
	
}
