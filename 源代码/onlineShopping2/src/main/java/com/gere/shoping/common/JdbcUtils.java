package com.gere.shoping.common;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * jdbc操作的公共类
 * @author cqyhm
 *
 */
public class JdbcUtils {
	
	private static String username="";
	private static String password="";
	private static String url="";
	private static Logger logger=LoggerFactory.getLogger(JdbcUtils.class);
	//静态块,在该类被类装载器载入的时候自动执行
	static {
		try {
			//读取配置文件
			Properties prop=new Properties();
			prop.load(JdbcUtils.class.getResourceAsStream("/jdbc.properties"));
			//获取配置参数
			username=prop.getProperty("jdbc.username"); //读取用户名
			password=prop.getProperty("jdbc.password"); //读取密码
			url=prop.getProperty("jdbc.url");           //读取链接地址
			//载入并初始化数据库驱动
			Class.forName(prop.getProperty("jdbc.driver"));
			logger.debug("初始化mysql驱动");
		} catch (ClassNotFoundException | IOException e) {
			logger.error("初始化驱动或读取参数异常:{}",e.getMessage());
			throw new CommException("初始化驱动或读取参数异常"+e.getMessage());
		}
	}
	/**
	 * 获取数据库链接
	 * @return
	 */
	public static Connection getConnection() {
		try {
			return DriverManager.getConnection(url, username, password);
		} catch (Exception e) {
			logger.error("获取数据库链接异常:{}",e.getMessage());
			return null;
		}
	}
	/**
	 * 关闭公共的资源
	 * @param conn 连接
	 * @param ps   语句
	 * @param rs   结果集
	 */
	public static void close(Connection conn,Statement ps,ResultSet rs) {
		try {
			if(rs!=null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error("关闭ResultSet资源异常:{}",e.getMessage());
		}finally {
			try {
				if(ps!=null) {
					ps.close();
				}
			} catch (Exception e2) {
				logger.error("关闭Statement资源异常:{}",e2.getMessage());
			} finally {
				try {
					if(conn!=null) {
						conn.close();
					}	
				} catch (Exception e3) {
					logger.error("关闭Connection资源异常:{}",e3.getMessage());
				}
			}
		}
	}
}
