package com.gere.shoping.common;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebFilter(urlPatterns="/*")
public class LoginFilter implements Filter{
	private Logger logger =LoggerFactory.getLogger(getClass());

	private String[] ignoreUrls=new String[] {"login","admin","registration","Lostpassword","assets","kaptcha","list.do","register.do"};

	/*private boolean ignore(String url) {
		for(int i=0;i<ignoreUrls.length;i++) {
			if(url.contains(ignoreUrls[i])) {
				return true;
			}
		}
		return false;
	}*/
	
	public void destroy() {
		
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req=(HttpServletRequest)request;
		HttpServletResponse resp=(HttpServletResponse)response;
        String url=req.getRequestURI(); //过滤的请求地址
		
		//检查是否是需要忽略的请求
		boolean ignore=false;
		for (String str : ignoreUrls) {
			if(url.contains(str)) {
				ignore=true;
				break;
			}
		}
		//不需要做登录检查的地址
		if(ignore) {
			logger.info("不需要登录判断的地址:{},直接通过",url);
			chain.doFilter(req, resp);
		}else { //需要判断登录的地址(已经登录的用户会在session中存在user的值)
			HttpSession session=req.getSession();
			Object user=session.getAttribute("user");
			if(user!=null) {
				//已经登录的直接通过
				chain.doFilter(req, resp);
				
			}else {
				//没有登录的跳转到登录页面
				
				resp.sendRedirect(req.getContextPath()+"/login.jsp");
			}
		}
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
		
	}

}
