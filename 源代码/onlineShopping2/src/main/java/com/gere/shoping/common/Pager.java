package com.gere.shoping.common;

import java.io.Serializable;

public class Pager implements Serializable {

	private static final long serialVersionUID = 4535549022740161096L;
	
	public Pager() {
		this.pageSize=5;
		this.currentPage=1;
	}
	public Pager(Integer totalSize) {
		this.totalSize=totalSize;
	}
	
	public Pager(Integer totalSize,Integer pageSize,Integer currentPage) {
		this.totalSize=totalSize;
		this.pageSize=pageSize;
		this.currentPage=currentPage;
	}
	/**
	 * 定义每一页显示多少条记录
	 */
	private Integer pageSize=5;
	/**
	 * 当前是哪一页
	 */
	private Integer currentPage=1;
	/**
	 * 总共的记录数
	 */
	private Integer totalSize=0;
	
	/**
	 * 查询记录的偏移位置
	 * @return
	 */
	public Integer getOffset() {
		return (getCurrentPage()-1)*getPageSize();
	}
	/**
	 * 总的页数
	 * @return
	 */
	public Integer getTotalPage() {
		if(totalSize%pageSize==0) {
			return totalSize/pageSize;
		}else {
			return totalSize/pageSize+1;
		}
	}
	/**
	 * 得到前一页
	 * @return
	 */
	public Integer getPrevPage() {
		if(getCurrentPage()>1) {
			return getCurrentPage()-1;
		}else {
			return 1;
		}
	}
	/**
	 * 得到后一页
	 * @return
	 */
	public Integer getNextPage() {
		if(getCurrentPage()>1&&getCurrentPage()<getTotalPage()) {
			return getCurrentPage()+1;
		}else {
			return getTotalPage();
		}
	}
	/**
	 * 获取每页记录数
	 * @return
	 */
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	/**
	 * 获取当前页
	 * @return
	 */
	public Integer getCurrentPage() {
		return currentPage<1?1:currentPage;
	}
	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}
	/**
	 * 获取总记录数
	 * @return
	 */
	public Integer getTotalSize() {
		return totalSize;
	}
	public void setTotalSize(Integer totalSize) {
		this.totalSize = totalSize;
	}
	
}
