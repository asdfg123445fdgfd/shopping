package com.gere.shoping.common;

import java.util.UUID;

public class StringUtils {
	
	private StringUtils() {}
	/**
	 * 生成唯一的UUID，并删除"-"符号，然后转换为大写
	 * @return
	 */
	public static String generateUUID() {
		return UUID.randomUUID().toString().replace("-", "").toUpperCase();
	}
}
