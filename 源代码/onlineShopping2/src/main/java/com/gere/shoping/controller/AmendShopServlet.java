/**
 * @author hxm
 * 商品的新增和修改
 */
package com.gere.shoping.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gere.shoping.common.CommException;
import com.gere.shoping.common.StringUtils;
import com.gere.shoping.domain.Shop;
import com.gere.shoping.services.ShopService;
@WebServlet(urlPatterns = "/amend.do")
@MultipartConfig(maxFileSize = 5 * 1024 * 1024)
public class AmendShopServlet extends HttpServlet{

	private static final long serialVersionUID = -2054879663664074070L;
	Logger logger = LoggerFactory.getLogger(getClass());
	private ShopService shopService = new ShopService();
	Shop shop =  new Shop();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			logger.debug("执行dopost");
			//获取传递的文件
			
			Part part=req.getPart("picture");
			if(part==null) {
				throw new CommException("必须选择上传的文件");
			}
			logger.debug("执行到part");
			
			String path="E:/周实训/picture"; //文件存放路径
			//保存在服务器上的文件名称,用来保证服务器上的文件名称唯一
			String filename=StringUtils.generateUUID()+"-"+part.getSubmittedFileName();
			
			part.write(path+"/"+filename); //保存文件
			logger.info("保存文件路径:{}/{}",path,filename);
			//获取参数
			String id = req.getParameter("id");
			String serial_num = req.getParameter("serial_num");
			String trade_num = req.getParameter("trade_num");
			String trade_name = req.getParameter("trade_name");
			String trade_carriage = req.getParameter("trade_carriage");
			String trade_price = req.getParameter("trade_price");
			String trade_brand = req.getParameter("trade_brand");
			String trade_type = req.getParameter("trade_type");
			String trade_size = req.getParameter("trade_size");
			String 	picture = "/" + filename;
			logger.debug("获取参数成功，商品编号：{}",serial_num);
			
			//类型转换
			int trade_num2 = Integer.parseInt(trade_num);
			logger.debug("获取{}",trade_num2);
			float trade_carriage2 = Float.parseFloat(trade_carriage);
			float trade_price2 = Float.parseFloat(trade_price);
			
			if(id == null || "".equals(id)) {
				shop.setId(null);
			}else{
				shop.setId(Integer.parseInt(id));
			}
			
			logger.debug("转换类型成功");
			// 封装
			shop.setSerial_num(serial_num);
			shop.setTrade_name(trade_name);
			shop.setTrade_num(trade_num2);
			shop.setTrade_brand(trade_brand);
			shop.setTrade_carriage(trade_carriage2);
			shop.setTrade_price(trade_price2);
			shop.setTrade_size(trade_size);
			shop.setTrade_type(trade_type);
			shop.setPicture(picture);
			
			//数据校验
			if (trade_name == null) {
				throw new CommException("商品名称不能为空");				
			}
			if (serial_num == null) {
				throw new CommException("商品编号不能为空");
			}
			if (trade_size == null) {
				throw new CommException("商品尺码不能为空");
			}
			
			
			//调用业务方法
			shopService.update(shop);
			
			HttpSession session=req.getSession();
			session.setAttribute("shop", shop);
			logger.debug("业务处理成功");

			//5.转发或者重定向	——转发	
			resp.sendRedirect(req.getContextPath()+"/list.do");
			logger.debug("编辑商品成功");

		} catch (Exception e) {
			logger.error(e.getMessage());
			req.setAttribute("error", e.getMessage());
			req.getRequestDispatcher("/amend.jsp").forward(req, resp);
		}
	}

	
}
