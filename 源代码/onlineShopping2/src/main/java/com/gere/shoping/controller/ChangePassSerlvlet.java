package com.gere.shoping.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gere.shoping.common.CommException;
import com.gere.shoping.common.StringUtils;
import com.gere.shoping.domain.User;
import com.gere.shoping.services.UserService;

@WebServlet(urlPatterns="/changepwd.do")
@MultipartConfig(maxFileSize = 5 * 1024 * 1024)
public class ChangePassSerlvlet extends HttpServlet{
   private UserService userservice =new UserService();
   private Logger logger =LoggerFactory.getLogger(getClass());
	private static final long serialVersionUID = 2423874074936627202L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		logger.debug("执行dopost");
		try {
			//获取传递的文件
			Part part=req.getPart("picture");
			logger.debug("执行到part");
			if(part == null) {
				throw new CommException("必须选择上传的文件");
			}
			String path="E:/周实训/picture"; //文件存放路径
			//保存在服务器上的文件名称,用来保证服务器上的文件名称唯一
			String filename=StringUtils.generateUUID()+"-"+part.getSubmittedFileName();
			
			part.write(path+"/"+filename); //保存文件
			logger.info("保存文件路径:{}/{}",path,filename);
			
			//获取参数
			String usercode =req.getParameter("usercode");
			
			String password = req.getParameter("password");
			String newpassword =req.getParameter("newpassword");
			String repassword =req.getParameter("repassword");
			String picture = "/" + filename;
			//数据转换
			//数据校验
			if(usercode==null||usercode.trim().length()<1) {
				throw new CommException("账号不能为空");
			}
			if(password==null||password.trim().length()<1) {
				throw new CommException("密码不能为空");
			}
			if(newpassword==null||newpassword.trim().length()<1) {
				throw new CommException("新密码不能为空");
			}
			if(!newpassword.equals(repassword)) {
				throw new CommException("输入的密码不一致");
			}
			User user =new User();
			user.setUsercode(usercode);
			user.setPassword(password);
			user.setNewpassword(newpassword);
			user.setPicture(picture);
			
			//调用业务
			 userservice.changpd(user);
			//跳转
			req.getRequestDispatcher("/login.jsp").forward(req, resp);

			//resp.sendRedirect(req.getContextPath()+"/login.jsp");
			logger.info("改变密码成功");
		} catch (Exception e) {
			e.printStackTrace();
			req.setAttribute("error", e.getMessage());
			req.getRequestDispatcher("/Chpassword.jsp").forward(req, resp);
			logger.error("改变密码失败:{}",e.getMessage());
		}
		
		
	}
    
	
	
}
