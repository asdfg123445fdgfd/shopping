package com.gere.shoping.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * 用于前台展示指定png图片文件
 * 调用格式: /download.do?filename=/image.png
 * 注意该servlet必须登录后才能访问
 * @author cqyhm
 *
 */
@WebServlet(urlPatterns= {"/download.do"})
public class DownloadServlet extends HttpServlet{

	private static final long serialVersionUID = 8478040686398359097L;
	private Logger logger=LoggerFactory.getLogger(getClass());
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String filename=req.getParameter("filename");//从页面获取文件 并把文件名给filename
		String path="E:/周实训/picture"; //图片存放的路径     ****这个路径必须存在，需要显示的文件必须在这个路径下***
		try { 
			if(filename != null && filename.trim().length() > 0) {
				logger.info("读取文件名:{}{}",path,filename);
				resp.setContentType("image/png");
				OutputStream out=resp.getOutputStream();//获取输入流
				//注意这里的文件一定要存在,不存在会报异常
				InputStream in=new FileInputStream(new File(path+filename));//打开文件流
				//读取输入文件流的内容，然后
				byte data[] = new byte[4096];// 缓冲字节数
	            int size = in.read(data);//读取图片一定数量字节数到缓冲区
	            while (size != -1) {
	                out.write(data, 0, size);
	                size = in.read(data);
	            }
	            in.close();
	            out.flush();
	            out.close();
	            logger.info("文件读取成功");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("读取图片文件错误:",e.getMessage());
		}
	}
}
