package com.gere.shoping.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.gere.shoping.common.CommException;

import com.gere.shoping.domain.User;
import com.gere.shoping.services.UserService;

@WebServlet(urlPatterns="/login.do")
public class LoginServletss extends HttpServlet{
    
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	private static final long serialVersionUID = -8186139119750074588L;
    private UserService userservice =new UserService();
    private Logger logger = LoggerFactory.getLogger(getClass());
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		try {
			logger.debug("执行dopost");
			//获取参数
			String usercode =req.getParameter("usercode");
			String password =req.getParameter("password");
			logger.debug("{},{}",usercode,password);
			 //类型转换
			//数据校验
			if(usercode==null||usercode.trim().length()<1) {
				throw new CommException("账号不能为空");
			}
			if(password==null||password.trim().length()<1) {
				throw new CommException("密码不能为空");
			}
			
			//跳转
			User user=userservice.login(usercode,password);
			HttpSession session=req.getSession();
			session.setAttribute("user", user);
			logger.debug("业务处理成功");
			//5.转发或者重定向
			//resp.sendRedirect(req.getContextPath() + "/index.jsp");
			req.getRequestDispatcher("/index.jsp").forward(req, resp);
		} catch (Exception e) {
			logger.debug("servlet密码错误");                                   
			logger.error(e.getMessage());                               
			req.setAttribute("error", e.getMessage());                  
			req.getRequestDispatcher("/login.jsp").forward(req, resp);  
		}                                                               
		
	}
    
}
