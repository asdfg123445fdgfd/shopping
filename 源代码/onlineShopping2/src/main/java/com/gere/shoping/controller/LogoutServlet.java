package com.gere.shoping.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet(urlPatterns= {"/user/logout.do"})
public class LogoutServlet extends HttpServlet{
	private Logger logger=LoggerFactory.getLogger(getClass());
	
	private static final long serialVersionUID = -7151864387203253072L;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		logger.info("logout.dopost");
		HttpSession session=req.getSession();
		session.invalidate();
		resp.sendRedirect(req.getContextPath()+"/login.jsp");
		logger.info("退出登录成功");
	}

	

}
