package com.gere.shoping.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gere.shoping.common.CommException;
import com.gere.shoping.domain.User;
import com.gere.shoping.services.UserService;

@WebServlet(urlPatterns="/Lostpassword.do")
public class LostpasswordServlet extends HttpServlet{
	private static final long serialVersionUID = 4684686468709422488L;
    private Logger logger=LoggerFactory.getLogger(getClass());
    private UserService userservice=new UserService();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		logger.debug("执行找密码dopost");
		try {
			 //获取参数
			String telephone=req.getParameter("telephone");
			String usercode=req.getParameter("usercode");
			String Email=req.getParameter("Email");
			String kaptcha=req.getParameter("kaptcha");
			//类型转换
			//数据校验
			logger.debug("获取参数{},{},{},{}",telephone,usercode,Email,kaptcha);
			
			if(telephone!=null&&telephone.trim().length()<11) {
				throw new CommException("手机号不能小于十一位");
			}
			if(usercode==null||usercode.trim().length()<5) {
				throw new CommException("不能为空");
			}
			if(Email==null||Email.trim().length()<1) {
				throw new CommException("不能为空");
			}
			if(kaptcha==null||kaptcha.trim().length()<0) {
				throw new CommException("验证码错误");
			}
			HttpSession session=req.getSession();
			Object object=session.getAttribute("kaptcha"); //获取session中的验证码
			if(!kaptcha.equals(object)) {
				throw new CommException("验证码输入错误");
			}
			
			//跳转
			User user=new User();
			user.setTelephone(telephone);
			user.setUsercode(usercode);
			user.setEmail(Email);
			User newInfos= userservice.findPasswrod(user);
			logger.debug("找回");
			req.setAttribute("newInfos", newInfos);
			req.getRequestDispatcher("/Findpassword.jsp").forward(req, resp);
			logger.info("查找成功");
		} catch (Exception e) {
			logger.debug("查找失败");
			logger.error(e.getMessage());
			req.setAttribute("error", e.getMessage());
			req.getRequestDispatcher("/Lostpassword.jsp").forward(req, resp);
		}
	}
   
	

}
