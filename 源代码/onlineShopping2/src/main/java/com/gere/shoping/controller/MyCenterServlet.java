/**
 * @author hxm
 * 个人中心的修改
 */
package com.gere.shoping.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gere.shoping.common.CommException;
import com.gere.shoping.common.StringUtils;
import com.gere.shoping.domain.User;
import com.gere.shoping.services.UserService;

@WebServlet(urlPatterns = {"/mycenter.do"})
@MultipartConfig(maxFileSize = 5 * 1024 * 1024)
public class MyCenterServlet extends HttpServlet{

	private static final long serialVersionUID = -4911924518164441508L;
	Logger logger = LoggerFactory.getLogger(getClass());
	User user = new User();
	UserService userService = new UserService();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			//获取传递的文件
			Part part=req.getPart("picture");
			logger.debug("执行到part");
			if(part == null) {
				throw new CommException("必须选择上传的文件");
			}
			String path="E:/周实训/picture"; //文件存放路径
			//保存在服务器上的文件名称,用来保证服务器上的文件名称唯一
			String filename=StringUtils.generateUUID()+"-"+part.getSubmittedFileName();
			
			part.write(path+"/"+filename); //保存文件
			logger.info("保存文件路径:{}/{}",path,filename);
			//1 获取参数
			String usercode = req.getParameter("usercode");
			String username = req.getParameter("username");
			String telephone = req.getParameter("telephone");
			String email = req.getParameter("email");
			String address = req.getParameter("address");
			String 	picture = "/" + filename;
			//2 数据转换
			
			//3 封装
			user.setUsercode(usercode);
			user.setUsername(username);
			user.setTelephone(telephone);
			user.setEmail(email);
			user.setAddress(address);
			user.setPicture(picture);
			
			//4 数据校验
			if (username == null) {
				username = null;
				logger.debug("用户昵称未修改");
			}
			if (telephone == null) {
				telephone = null;
				logger.debug("用户电话未修改");
			}
			if (email == null) {
				email = null;
				logger.debug("email修改");
			}
			if (address == null) {
				address = null;
				logger.debug("用户地址未修改");
				
			}
			
			//5 业务调用方法
			userService.updateInfo(user);
			logger.debug("业务调用方法成功");
			HttpSession session=req.getSession();
			session.setAttribute("user", user);
			logger.debug("业务处理成功");
			
			//6 转发或重定向
			//resp.sendRedirect(req.getContextPath() + "/index.do");
			req.getRequestDispatcher("/index.jsp").forward(req, resp);  

		} catch (Exception e) {
			//7 异常处理
			logger.error(e.getMessage());                               
			req.setAttribute("error", e.getMessage());                  
			req.getRequestDispatcher("/mycenter.jsp").forward(req, resp);  
			                                                            
			

		}

	}

}
