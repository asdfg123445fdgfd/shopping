package com.gere.shoping.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gere.shoping.common.CommException;
import com.gere.shoping.services.ShopService;




@WebServlet(urlPatterns= {"/delete.do"})
public class ShopDeleteServlet extends HttpServlet{

	
	private static final long serialVersionUID = 1848512979485511252L;
	private Logger logger=LoggerFactory.getLogger(getClass());
	private ShopService shopService=new ShopService();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("11111111");
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			logger.debug("执行doPost");
			//1.获取惨呼
			String id=req.getParameter("id");
			//2.类型转换
			//3.数据校验
			if(id==null || id.length()<1) {
				throw new CommException("删除时必须传递id");
			}
			int idd=Integer.parseInt(id);
			//4.调用业务方法
			shopService.delete(idd);
			//5.跳转
			req.getRequestDispatcher("/list.do").forward(req, resp);
			logger.debug("删除完成");
		} catch (Exception e) {
			//5.跳转
			logger.error(e.getMessage());
			req.setAttribute("error", e.getMessage());
			req.getRequestDispatcher("/list.jsp").forward(req, resp);
		}
	}

}
