package com.gere.shoping.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gere.shoping.domain.Shop;
import com.gere.shoping.services.ShopService;



/**
 * 处理查找单个新闻的请求
 * @author cqyhm
 */
@WebServlet(urlPatterns= {"/shop/findbyid.do","/shop/edit.do","/shop/add.do"})
public class ShopFindServlet extends HttpServlet{

	private static final long serialVersionUID = 3094565686266577779L;
	private Logger logger=LoggerFactory.getLogger(getClass());
	private ShopService shopService=new ShopService();
	/**
	 * 处理get方法发送过来的请求
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	/**
	 * 处理post方法发送过来的请求
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		logger.info("shopfind.dopost");
		try {
			//1.接收前台传递过来的参数
			String id=req.getParameter("id"); //有id则是修改，没有id则是新增
			//2.参数类型转换
			//3.参数合法性校验
			//4.参数封装为对象
			//5.调用业务方法
			//6.转发或重定向(可传递数据给页面)
			if(id==null||id.trim().length()<=0) {
				resp.sendRedirect(req.getContextPath()+"/edit.jsp");
				return;
			}else {
				Shop shop=shopService.findById(Integer.parseInt(id));
				req.setAttribute("shop", shop);
				req.getRequestDispatcher("/amend.jsp").forward(req, resp);
			}
			logger.info("进入编辑页面成功");
		} catch (Exception e) {
			e.printStackTrace();
			req.setAttribute("error", e.getMessage());
			req.getRequestDispatcher("/edit.jsp").forward(req, resp);
			logger.error("进入编辑页面失败:{}",e.getMessage());
		}
	}

}
