package com.gere.shoping.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gere.shoping.common.Pager;
import com.gere.shoping.domain.Shop;
import com.gere.shoping.services.ShopService;





@WebServlet(urlPatterns="/list.do")
public class ShopsListServlet extends HttpServlet{

	private static final long serialVersionUID = -7271041558224551183L;
	private Logger logger =LoggerFactory.getLogger(getClass());
	private ShopService newService=new ShopService();
	
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		logger.debug("执行dopost_list");
		try {
			
			//接受参数
			String serial_num = req.getParameter("serial_num");
			String trade_type=req.getParameter("trade_type");
			
			String pageSize=req.getParameter("pageSize");
			String currentPage=req.getParameter("currentPage");
			pageSize=pageSize==null?"5":pageSize;
			currentPage=currentPage==null?"1":currentPage;
			
			Shop shop = new Shop();
			shop.setSerial_num(serial_num);
			shop.setTrade_type(trade_type);
			Pager pager=new Pager();
			
			if(pageSize==null||pageSize.length()<1) {
				pager.setPageSize(5);
			}else {
				pager.setPageSize(Integer.valueOf(pageSize));
			}
			if(currentPage==null||currentPage.length()<1) {
				pager.setCurrentPage(1);
			}else {
				pager.setCurrentPage(Integer.valueOf(currentPage));
			}
			if(serial_num==null) {
				shop.setSerial_num("");
			}
			if(trade_type==null) {
				shop.setTrade_type("");
			}
			//数据校验
			//类型转换
			//执行业务
			List<Shop> shopp=newService.findPager(shop,pager);
			//跳转
			req.setAttribute("shopp", shopp);
			req.setAttribute("pager", pager);
			req.getRequestDispatcher("/list.jsp").forward(req, resp);
			logger.debug("新闻分页查询成功");
		}catch (Exception e) {
			//跳转
			logger.error(e.getMessage());
			req.setAttribute("error", e.getMessage());
			req.getRequestDispatcher("/list.jsp").forward(req, resp);
		}
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

}
