package com.gere.shoping.controller;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gere.shoping.common.CommException;

import com.gere.shoping.domain.User;
import com.gere.shoping.services.UserService;

@WebServlet(urlPatterns= "/register.do")
public class registrationServletss extends HttpServlet{
    private Logger logger=LoggerFactory.getLogger(getClass());
	private static final long serialVersionUID = -8186139119750074588L;
    private UserService userservice=new UserService();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
    
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		logger.debug("执行dopost");
		try {
			//获取参数
			logger.debug("开始执行");
			String usercode=req.getParameter("usercode");
			String username=req.getParameter("username");
			String Email=req.getParameter("Email");
			String password=req.getParameter("password");
			String repassword=req.getParameter("repassword");
			String telephone=req.getParameter("telephone");
			String type=req.getParameter("type");
			//类型转换
			int types=Integer.parseInt(type);
			//数据校验
			if(usercode==null||usercode.trim().length()<=0) {
				   throw new com.gere.shoping.common.CommException("密码不能为空");
				}
				//用户不能为空
				if(username==null||username.trim().length()<=0) {
					throw new com.gere.shoping.common.CommException("用户不能为空");
				}
				if(Email==null||Email.trim().length()<=0) {
					throw new com.gere.shoping.common.CommException("邮箱不能为空");
				}
				if(password==null||password.trim().length()<=0) {
					throw new com.gere.shoping.common.CommException("密码不能为空");
				}
				//新密码不能为空
				if(repassword==null||repassword.trim().length()<=0) {
					throw new com.gere.shoping.common.CommException("新密码不能为空");
				}
				if(telephone==null||telephone.trim().length()<=0) {
					throw new CommException("电话号码不能为空");
				}
				if(!password.equals(repassword)) {
					throw new CommException("两次输入密码不一致");
				}

			    if(!(types != 0 || types != 1)) {
			    	logger.debug("类型为{}",types);
			    }
               logger.debug("校验成功");
			    if(!(types!=0||types!=1)) {
			    	throw new CommException("请输入正确格式");
			    }
				User user =new User();
				user.setUsercode(usercode);
				user.setUsername(username);
				user.setPassword(repassword);
				user.setEmail(Email);
				user.setTelephone(telephone);
				user.setType(types);
				//业务跳转
				userservice.register(user);
				resp.sendRedirect(req.getContextPath()+"/login.jsp");//重定向
				logger.info("注册成功");
		}catch (Exception e) {
			    //失败跳转
			    req.setAttribute("error", e.getMessage());
			    req.getRequestDispatcher("/registration.jsp").forward(req, resp); //转发
			    logger.error("注册失败：{}e",e.getMessage());
		}
		
	}
   
}
