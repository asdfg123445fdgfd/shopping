package com.gere.shoping.controller;

import java.util.List;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gere.shoping.domain.Shop;
import com.gere.shoping.services.ShopService;

@WebServlet(urlPatterns= {"/shop/List.do"})
public class shopListServlet extends HttpServlet{
	
    private ShopService shopservice= new ShopService();
	private static final long serialVersionUID = 2118050568926691681L;
    private Logger logger=LoggerFactory.getLogger(getClass());
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		     doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		  try {
			      //获取参数
				  String serial_num=req.getParameter("serial_num");
				  serial_num=serial_num==null?"":serial_num;//转为空字符
				  
				  //收集参数
				  Shop shop=new Shop();
				  shop.setSerial_num(serial_num);
				  //校验参数
				  //业务跳转
			    List<Shop>  shops=shopservice.findshop(shop);
			    //转发
			    req.setAttribute("shops", shops);
			    logger.debug("{}",shops.size());
			    req.getRequestDispatcher("/shop.jsp").forward(req, resp);
		} catch (Exception e) {
			req.setAttribute("error", e.getMessage());
			req.getRequestDispatcher("/index.jsp").forward(req, resp);
		}
		  
		  
	}
        
}
