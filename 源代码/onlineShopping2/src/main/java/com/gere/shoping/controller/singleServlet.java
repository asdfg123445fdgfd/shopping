package com.gere.shoping.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gere.shoping.domain.Shop;
import com.gere.shoping.services.ShopService;

@WebServlet(urlPatterns= {"/user/sing.do"})
public class singleServlet extends HttpServlet{
	private ShopService shopservice=new ShopService();
	private Logger logger = LoggerFactory.getLogger(getClass());
	private static final long serialVersionUID = 8474435870626518404L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		logger.debug("执行lookdopost");
		try {
			//获取参数
			String id=req.getParameter("id");
			logger.debug("{}",id);
			 //数据转换

			int ids= Integer.parseInt(id); 

			//数据校验
			//调用业务
			Shop shopv=shopservice.findcountss(ids);
			req.setAttribute("shopv", shopv);
			req.getRequestDispatcher("/shop.jsp").forward(req, resp);
			logger.debug("数据插入成功");
		} catch (Exception e) {
			req.setAttribute("error", e.getMessage());
			req.getRequestDispatcher("/index.jsp").forward(req, resp);
		}
	}

	
}
