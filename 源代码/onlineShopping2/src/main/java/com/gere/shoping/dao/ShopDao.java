package com.gere.shoping.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.gere.shoping.common.CommException;
import com.gere.shoping.common.JdbcUtils;
import com.gere.shoping.common.Pager;
import com.gere.shoping.domain.Shop;

public class ShopDao {
    
private Logger logger=LoggerFactory.getLogger(getClass());


	private Shop rowMap(ResultSet rs) throws SQLException {
		Shop shop = new Shop();
		shop.setId(rs.getInt("id"));
		shop.setTrade_type(rs.getString("trade_type"));
		shop.setPicture(rs.getString("picture"));
		shop.setTrade_price(rs.getFloat("trade_price"));
		shop.setTrade_num(rs.getInt("trade_num"));
		shop.setTrade_name(rs.getString("trade_name"));
		shop.setTrade_brand(rs.getString("trade_brand"));
		shop.setTrade_size(rs.getString("trade_size"));
		shop.setTrade_carriage(rs.getFloat("trade_carriage"));
		shop.setSerial_num(rs.getString("serial_num"));
		shop.setPicture(rs.getString("picture"));
		return shop;
	}
	
	public List<Shop> findshop(Shop shop){
		//1.获取数据库链接
				Connection conn=JdbcUtils.getConnection();
				PreparedStatement ps=null;
				ResultSet rs=null;
				try {
					//2.准备sql语句
					String sql="select * from trades where trade_num like ? ";
					//查询并设置根据条件得到的记录数
					
					ps=conn.prepareStatement(sql); 
					//3.给sql语句设置参数
					ps.setString(1, "%"+shop.getSerial_num()+"%");
					logger.info("{}",sql);
					//4.执行sql语句
					rs=ps.executeQuery();
					//5.处理sql执行的结果
					List<Shop> list=new ArrayList<>();
					while (rs.next()) {
						list.add(rowMap(rs));
					}
					return list;
				} catch (Exception e) {
					throw new CommException("查询数据:"+e.getMessage());
				}finally {
					//6.关闭资源(链接、语句、结果集)
					JdbcUtils.close(conn, ps, rs);
				}
		
	}
     
	
	
	public Shop findcount(Integer id) {
		logger.debug("查询中1");
		Connection conn = JdbcUtils.getConnection();
		PreparedStatement ps=null;
		ResultSet rs = null;
		try {
			//2.准备sql语句
			logger.debug("查询中2");
			String sql="select * from trades where id=?";
			ps=conn.prepareStatement(sql);
			logger.debug("执行sql成功");
			//3.给sql语句设置参数


			ps.setInt(1,id );
			


			logger.info("{}",sql);
			//4.执行sql语句
			rs=ps.executeQuery();
			//5.处理sql执行的结果
			logger.debug("查询中3");
			while (rs.next()) {
				return rowMap(rs);
				
			}

			return null;
		} catch (Exception e) {
			throw new CommException("查询异常");
		}finally {
			//6.关闭资源(链接、语句、结果集)
			JdbcUtils.close(conn, ps, rs);
			logger.debug("查询中4");
		}
		
	}


	/**
	 * 商品的新增
	 * @param shop
	 */
	public void insert_shop(Shop shop) {
		
		Connection conn = JdbcUtils.getConnection();
		PreparedStatement ps=null;
		ResultSet rs = null;
		String sql = null;
		try {
			//2.准备sql语句
			sql = "INSERT INTO trades(serial_num,trade_num,trade_name,trade_carriage,trade_price,trade_brand,trade_type,trade_size,picture)" + 
					"VALUES(?,?,?,?,?,?,?,?,?)";
			logger.error("sql:{}",sql);
			logger.debug("sql执行成功");
			ps=conn.prepareStatement(sql);
			//3.给sql语句设置参数
			ps.setString(1, shop.getSerial_num());
			ps.setInt(2, shop.getTrade_num());
			ps.setString(3, shop.getTrade_name());
			ps.setFloat(4, shop.getTrade_carriage());
			ps.setFloat(5, shop.getTrade_price());
			ps.setString(6, shop.getTrade_brand());
			ps.setString(7, shop.getTrade_type());
			ps.setString(8, shop.getTrade_size());
			ps.setString(9, shop.getPicture());
			logger.info("{}",sql);
			//5.处理sql执行的结果
			if(ps.executeUpdate()<1) {
				throw new CommException("新增商品失败");
			}
			
		} catch (Exception e) {
			throw new CommException("商品新增被抛出");
		}finally {
			//5.关闭资源
			JdbcUtils.close(conn, ps, rs);
			logger.debug("商品新增关闭资源");
		}
	}
	public void insert_cart(Shop shop1) {
		 logger.debug("插入中");
		//1.获取数据库链接
				Connection conn=JdbcUtils.getConnection();
				PreparedStatement ps=null;
				ResultSet rs=null;
				try {
					//2.准备sql语句
					String sql="insert into cart(serial_num,trade_price,trade_num,trade_name,trade_size,picture) values(?,?,?,?,?,?)";
					ps=conn.prepareStatement(sql);
					//3.准备sql语句的参数
					ps.setString(1, shop1.getSerial_num());
				    ps.setFloat(2, shop1.getTrade_price());
					ps.setInt(3, shop1.getTrade_num());
					ps.setString(4, shop1.getTrade_name());
					ps.setString(5, shop1.getTrade_size());
					ps.setString(6, shop1.getPicture());
					logger.info("{}",sql);
					logger.debug("插入中2");
					//4.执行sql语句并处理返回结果
					if(ps.executeUpdate()!=1) {
						throw new CommException("插入数据错误");
					}
					logger.debug("插入中3");
				} catch (Exception e) {
					throw new CommException("插入数据异常:"+e.getMessage());
				}finally {
					//5.关闭资源
					JdbcUtils.close(conn, ps, rs);
					logger.debug("插入中4");
				}
		
	}

	
	
	
	public List<Shop> findshopcart(Shop shopcart) {
		//1.获取数据库链接
		Connection conn=JdbcUtils.getConnection();
		PreparedStatement ps=null;
		ResultSet rs=null;
		try {
			//2.准备sql语句
			String sql="select * from cart where trade_num like ? ";
			//查询并设置根据条件得到的记录数
			
			ps=conn.prepareStatement(sql); 
			//3.给sql语句设置参数
			ps.setString(1, "%"+shopcart.getSerial_num()+"%");
			
	         
			logger.info("{}",sql);
			//4.执行sql语句
			rs=ps.executeQuery();
			//5.处理sql执行的结果
			List<Shop> list=new ArrayList<>();
			while (rs.next()) {
				list.add(rowMaps(rs));
			}
			return list;
		} catch (Exception e) {
			throw new CommException("查询数据:"+e.getMessage());

		}finally {
			//6.关闭资源(链接、语句、结果集)
			JdbcUtils.close(conn, ps, rs);
		}
		
	}
	private Shop rowMaps(ResultSet rs) throws SQLException {
		Shop shop = new Shop();
		shop.setSerial_num(rs.getString("serial_num"));
		shop.setTrade_price(rs.getFloat("trade_price"));
		shop.setTrade_num(rs.getInt("trade_num"));
		shop.setTrade_name(rs.getString("trade_name"));
		shop.setPicture(rs.getString("picture"));
		return shop;
	}

	/**
	 * 根据商品名称修改
	 * @param shop
	 */
	public void update(Shop shop) {
		Connection conn=JdbcUtils.getConnection();
		PreparedStatement ps=null;
		ResultSet rs=null;
		try {
			String sql="update trades set trade_num=?,trade_name=?,"
					+ "trade_carriage=?,trade_price=?,trade_brand=?,trade_type=?,"
					+ "trade_size=?,picture=?"
					+ " where serial_num=?";
			ps=conn.prepareStatement(sql);
			ps.setInt(1, shop.getTrade_num());
			ps.setString(2, shop.getTrade_name());
			ps.setFloat(3, shop.getTrade_carriage());
			ps.setFloat(4, shop.getTrade_price());
			ps.setString(5, shop.getTrade_brand());
			ps.setString(6, shop.getTrade_type());
			ps.setString(7, shop.getTrade_size());
			ps.setString(8, shop.getPicture());
			ps.setString(9, shop.getSerial_num());
			
			logger.debug(sql);
			
			if(ps.executeUpdate()<1) {
				throw new CommException("修改数据失败");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new CommException(e.getMessage());
		} finally {
			JdbcUtils.close(conn, ps, rs);
		}
	}
	/**
	 * 根据参数查询总的记录数
	 * @param news
	 * @return
	 */
	public Integer findCount(Shop shop) {
		Connection conn=JdbcUtils.getConnection();
		PreparedStatement ps=null;
		ResultSet rs=null;
		try {
			String sql="select count(1) from trades where serial_num like ? and trade_type like ?";
			ps=conn.prepareStatement(sql);
			ps.setString(1, "%"+shop.getSerial_num()+"%");
			ps.setString(2, "%"+shop.getTrade_type()+"%");
			
			logger.debug(sql);
			
			rs=ps.executeQuery();
			
			while (rs.next()) {
				return rs.getInt(1);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new CommException(e.getMessage());
		} finally {
			JdbcUtils.close(conn, ps, rs);
		}
		return 0;
	}

	/**
	 * 商品管理界面
	 * @param shop
	 * @param pager
	 * @return
	 */
	public  List<Shop> findPager(Shop shop, Pager pager) {
		Connection conn=JdbcUtils.getConnection();
		PreparedStatement ps=null;
		ResultSet rs=null;
		try {
			String sql="select * from trades where serial_num like ? and trade_type like ?"
					+ " limit ?,?";
			ps=conn.prepareStatement(sql);
			ps.setString(1, "%"+shop.getSerial_num()+"%");
			ps.setString(2, "%"+shop.getTrade_type()+"%");
			ps.setInt(3, (pager.getCurrentPage()-1)*pager.getPageSize());
			ps.setInt(4, pager.getPageSize());
			
			logger.debug(sql);
			
			rs=ps.executeQuery();
			List<Shop> shopp=new ArrayList<>();
			while (rs.next()) {
				shopp.add(rowMap(rs));
			}
			pager.setTotalSize(findCount(shop));
			return shopp;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new CommException(e.getMessage());
		} finally {
			JdbcUtils.close(conn, ps, rs);
		}
	}
	/**
	 * 根据商品名称和类型模糊查询数据列表
	 * @param Shop
	 * @return
	 */
	public List<Shop> findList(Shop shop) {
		Connection conn=JdbcUtils.getConnection();
		PreparedStatement ps=null;
		ResultSet rs=null;
		try {
			String sql="select * from trades where serial_num like ? and trade_type like ?";
			ps=conn.prepareStatement(sql);
			ps.setString(1, "%"+shop.getSerial_num()+"%");
			ps.setString(2, "%"+shop.getTrade_type()+"%");
			
			logger.debug(sql);
			
			rs=ps.executeQuery();
			List<Shop> shopp=new ArrayList<>();
			while (rs.next()) {
				shopp.add(rowMap(rs));
			}
			return shopp;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new CommException(e.getMessage());
		} finally {
			JdbcUtils.close(conn, ps, rs);
		}
	}

	public void delete(int idd) {
		Connection conn=JdbcUtils.getConnection();
		PreparedStatement ps=null;
		ResultSet rs=null;
		try {
			String sql="delete from trades where id=?";
			ps=conn.prepareStatement(sql);
			ps.setLong(1, idd);
			
			logger.debug(sql);
			
			if(ps.executeUpdate()>1) {
				throw new CommException("删除数据失败");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new CommException(e.getMessage());
		} finally {
			JdbcUtils.close(conn, ps, rs);
		}
		
	}

	public Shop findById(int id) {
		//1.获取数据库链接
				Connection conn=JdbcUtils.getConnection();
				PreparedStatement ps=null;
				ResultSet rs=null;
				try {
					//2.准备sql语句
					String sql="select * from trades where id=?";
					ps=conn.prepareStatement(sql);
					//3.给sql语句设置参数
					ps.setLong(1, id);
					
					logger.info("{}",sql);
					//4.执行sql语句
					rs=ps.executeQuery();
					//5.处理sql执行的结果
					while (rs.next()) {
						return rowMap(rs);
					}
					return null;
				} catch (Exception e) {
					throw new CommException("插入数据异常:"+e.getMessage());
				}finally {
					//6.关闭资源(链接、语句、结果集)
					JdbcUtils.close(conn, ps, rs);
				}
			}
	}


