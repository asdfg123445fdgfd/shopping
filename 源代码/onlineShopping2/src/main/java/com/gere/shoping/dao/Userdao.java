package com.gere.shoping.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gere.shoping.common.CommException;
import com.gere.shoping.common.JdbcUtils;
import com.gere.shoping.domain.User;


public class Userdao {
    private Logger logger =LoggerFactory.getLogger(getClass());
    
    /**
     * 用户注册包括：usercode,username,password,usertype,Email,telephone
     * @param user
     */
	public void insert(User user) {
		Connection conn=JdbcUtils.getConnection();
		PreparedStatement ps=null;
		ResultSet rs=null;
		String sql = null;
		try {
			sql="insert into user(usercode,username,password,usertype,Email,telephone) values(?,?,?,?,?,?)";
			ps=conn.prepareStatement(sql);
			//3.准备sql语句的参数
			ps.setString(1, user.getUsercode());
			ps.setString(2, user.getUsername());
			ps.setString(3, user.getPassword());
			ps.setInt(4, user.getType());
			ps.setString(5, user.getEmail());
			ps.setString(6, user.getTelephone());
			
			logger.info("{}",sql);
			//4.执行sql语句并处理返回结果
			if(ps.executeUpdate() < 1) {
				throw new CommException("用户注册失败");
			}
		} catch (Exception e) {
			throw new CommException("用户注册被抛出");
		}finally {
			JdbcUtils.close(conn, ps, rs);
			logger.debug("用户注册关闭资源成功");
		}
	}
	/**
	 * 根据用户账号查找用户信息
	 * @param usercode
	 * @return
	 */
	public User findBycode(String usercode) {
		Connection conn =JdbcUtils.getConnection();
		PreparedStatement ps =null;
		ResultSet rs =null;
		try {
			String sql="select * from user where usercode=?";
			ps=conn.prepareStatement(sql);
			ps.setString(1, usercode);
			
			logger.debug(sql);
			
			rs=ps.executeQuery();
			while (rs.next()) {
				return rowMap(rs);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new CommException(e.getMessage());
		} finally {
			JdbcUtils.close(conn, ps, rs);
		}
		return null;
	}
	private User rowMap(ResultSet rs) throws SQLException {
		User user =new User();
		user.setId(rs.getLong("id"));
		user.setPassword(rs.getString("password"));
		user.setUsername(rs.getString("username"));
		user.setUsercode(rs.getString("usercode"));
		user.setTelephone(rs.getString("telephone"));
		user.setEmail(rs.getString("email"));
		user.setPicture(rs.getString("picture"));
		return user;
	}
	
	/**
	 * 修改用户密码
	 * @param user
	 */
	public void update(User user) {
		Connection conn=JdbcUtils.getConnection();
		PreparedStatement ps=null;
		ResultSet rs=null;
		try {
			//2.准备sql语句
			String sql="update user set username = ?,password = ?,picture = ? where usercode=?";
			ps=conn.prepareStatement(sql);
			//3.准备sql语句的参数
			ps.setString(1, user.getUsername());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getPicture());
			ps.setString(4, user.getUsercode());
			logger.info("{}",sql);
			//4.执行sql语句并处理结果
			if(ps.executeUpdate()!=1) {
				throw new CommException("修改数据错误");
			}
		} catch (Exception e) {
			throw new CommException("修改数据异常:"+e.getMessage());
		}finally {
			//5.关闭资源(链接、语句、结果集)
			JdbcUtils.close(conn, ps, rs);
		}
		
	}
	/**
	 * 更新用户信息，包括usercode，username，telephone，Email，address，picture
	 * @param user
	 */
	public void updateInfo(User user){
		Connection conn = JdbcUtils.getConnection();
		PreparedStatement ps=null;
		ResultSet rs = null;
		String sql = null;
		try {
			//2.准备sql语句
			sql = "UPDATE USER SET username = ?,telephone = ?,Email = ?,address = ?, picture = ? WHERE usercode = ?";
			logger.debug("sql执行成功");
			ps=conn.prepareStatement(sql);
			//3.给sql语句设置参数
			ps.setString(1, user.getUsername());
			ps.setString(2, user.getTelephone());
			ps.setString(3, user.getEmail());
			ps.setString(4, user.getAddress());	
			ps.setString(5, user.getPicture());
			ps.setString(6, user.getUsercode());
			logger.info("{}",sql);
			
			//5.处理sql执行的结果
			if(ps.executeUpdate()<1) {
				throw new CommException("用户修改失败");
			}			
		} catch (Exception e) {
			throw new CommException("用户修改被抛出" + e.getMessage());
		}finally {
			//关闭资源
			JdbcUtils.close(conn, ps, rs);
			logger.debug("用户修改关闭资源成功");
		}
		
	}
		
	
	
    
}
