package com.gere.shoping.domain;


import java.io.Serializable;

public class Shop implements Serializable{

	
	private static final long serialVersionUID = -3645799540533017500L;
	//id号
    private Integer id;
    //商品编号
    private String serial_num;
    //商品数量
    private int trade_num;
    //商品名称
    private String trade_name;
    //商品邮费
    private float trade_carriage;
    //商品单价
    private float trade_price;
    //商品品牌
    private String trade_brand;
    //商品类型
    private String trade_type;
    //商品风格
    private String trade_style;
    //商品尺码
    private String trade_size;
    //图片
    private String picture;

	public String getSerial_num() {
		return serial_num;
	}
	public void setSerial_num(String serial_num) {
		this.serial_num = serial_num;
	}
	
	public String getTrade_name() {
		return trade_name;
	}
	public void setTrade_name(String trade_name) {
		this.trade_name = trade_name;
	}
	
	public String getTrade_brand() {
		return trade_brand;
	}
	public void setTrade_brand(String trade_brand) {
		this.trade_brand = trade_brand;
	}
	
	public String getTrade_style() {
		return trade_style;
	}
	public void setTrade_style(String trade_style) {
		this.trade_style = trade_style;
	}
	public String getTrade_size() {
		return trade_size;
	}
	public void setTrade_size(String trade_size) {
		this.trade_size = trade_size;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public int getTrade_num() {
		return trade_num;
	}
	public void setTrade_num(int trade_num) {
		this.trade_num = trade_num;
	}
	public float getTrade_carriage() {
		return trade_carriage;
	}
	public void setTrade_carriage(float trade_carriage) {
		this.trade_carriage = trade_carriage;
	}
	public float getTrade_price() {
		return trade_price;
	}
	public void setTrade_price(float trade_price) {
		this.trade_price = trade_price;
	}
	public String getTrade_type() {
		return trade_type;
	}
	public void setTrade_type(String trade_type) {
		this.trade_type = trade_type;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

    
    
}
