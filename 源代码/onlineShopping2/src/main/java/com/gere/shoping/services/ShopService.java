package com.gere.shoping.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.gere.shoping.common.CommException;
import com.gere.shoping.common.Pager;
import com.gere.shoping.dao.ShopDao;
import com.gere.shoping.domain.Shop;
import com.gere.shoping.domain.User;

public class ShopService {
    private ShopDao shopdao= new ShopDao();
    private Logger logger = LoggerFactory.getLogger(getClass());
    
	public List<Shop> findshop(Shop shop) {
		return shopdao.findshop(shop);
	}
	public void findcount(Integer id) {
		logger.debug("xuuu");
		Shop shop1=shopdao.findcount(id);
		   logger.debug("{}",shop1.getId());
		  if(shop1.getId()==null) {
			  throw new CommException("物品不存在");
		  }
		  logger.debug("准备插入");
		  shopdao.insert_cart(shop1);
		
	}
	public List<Shop> findshopcart(Shop shopcart) {
		
		return shopdao.findshopcart(shopcart);
	}
	


	public Shop findcountss(Integer id) {		
		return shopdao.findcount(id);
	}
	/**
	 * 包括商品的新增和修改
	 * 根据页面传来的id判断当前是修改页还是新增页
	 * @param shop
	 */
	public void save(Shop shop) {
		shopdao.insert_shop(shop);
		
		
	}
	public List<Shop> findPager(Shop shop, Pager pager) {
		return shopdao.findPager(shop,pager);
	}
	public void delete(int idd) {
		shopdao.delete(idd);
	}
	public void update(Shop shop) {
		shopdao.update(shop);
	}
	public Shop findById(int id) {
		return shopdao.findById(id);
	}
	
	
	


}
