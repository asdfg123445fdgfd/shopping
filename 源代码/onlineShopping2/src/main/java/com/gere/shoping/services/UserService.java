package com.gere.shoping.services;


import com.gere.shoping.common.CommException;
import com.gere.shoping.dao.Userdao;
import com.gere.shoping.domain.User;

public class UserService {
   private Userdao userdao =new Userdao();
	public void register(User user) {
		userdao.insert(user);
		
	}
	
	public User login(String usercode, String password) {
		//调用数据存储方法从数据库获取账号信息
		
			User user =userdao.findBycode(usercode);
			if(user==null||user.getPassword()==null) {
				throw new com.gere.shoping.common.CommException("找不到对应帐号");
			}
			if(!password.equals(user.getPassword())) {
				throw new com.gere.shoping.common.CommException("svervice密码错误");
			}
			return user;
			
	}

	public User findPasswrod(User user) {
		User user3=userdao.findBycode(user.getUsercode());
		if(!user.getUsercode().equals(user3.getUsercode())) {
			throw new CommException("输入错误");
		}
		if(!user.getEmail().equals(user3.getEmail())) {
			throw new CommException("输入错误");
		}
		
		return user3;
	}

	public User changpd(User user) {
		User user2=userdao.findBycode(user.getUsercode());
		if(user2.getUsercode()==null) {
			throw new CommException("该用户不存在");
		}
		if(!user.getPassword().equals(user2.getPassword())) {
			throw new CommException("密码输入错误");
		}
		user2.setPassword(user.getNewpassword());
		userdao.update(user2);
		return user;
	}

	/**根据用户账号对用户信息进行更新
	 * 更新用户信息，包括usercode，username，telephone，Email，address，picture
	 * @param user
	 * @param usercode 
	 */
	public void updateInfo(User user) {
		userdao.updateInfo(user);
		
	}

	

	

}
