<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="renderer" content="webkit">
		<meta name="applicable-device" content="pc,mobile">
	<title>修改密码</title>
	<link href="${pageContext.request.contextPath}/assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/allstyle.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/menu.css" rel="stylesheet" type="text/css" media="all" />
	<script src="${pageContext.request.contextPath}/assets/js/menu.js" type="text/javascript" ></script>
	<script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/simpleCart.min.js"> </script>
<!-- start menu -->

<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>

	<!-- start menu -->
</head>
<body>
<jsp:include page="header.jsp"/>
<div class="blog">
	 <div class="container">
		 <ol class="breadcrumb">
		  <li><a href="${pageContext.request.contextPath}/index.jsp">主页</a></li>
		  <li class="active">个人中心</li>
		 </ol>
	 <h2>修改密码</h2>
	 <div class="col-md-9 fashion-blogs">
		 <div class="comments-main">
			 <div class="col-md-6 reg-form" style="float: left">
				 <div class="reg1">
					 <p style="margin-top: 25px">欢迎，请输入以下内容继续...</p>
					 <form action="${pageContext.request.contextPath}/changepwd.do" method="post" class="form-horizontal" role="form" enctype="multipart/form-data">
						 <ul>
						 	<li class="text-info">用户昵称:</li>
						    <li><input type="text" name = "username" value = "${user.username}" readonly = "readonly" ></li>
						 </ul>
						 <ul>
							 <li class="text-info">手机号码:</li>
							 <li><input type="text" value="${user.telephone}" readonly="readonly" name="telephone" placeholder="不得少于11位数"></li>
						 </ul>
						 <ul>
							 <li class="text-info">用户账号: </li>
							 <li><input type="text" value="${user.usercode}" readonly="readonly" name="usercode" placeholder="不得小于六位数" required></li>
						 </ul>
						 <ul>
							 <li class="text-info">用户旧密码: </li>
							 <li><input type="password" value="" name="password" placeholder="不得小于六位数" required></li>
						 </ul>
						 <ul>
							 <li class="text-info">用户新密码:</li>
							 <li><input type="password" value="" name="repassword" placeholder="不得小于六位数" required></li>
						 </ul>
						 <ul>
							 <li class="text-info">确认新密码:</li>
							 <li><input type="password" value="" name="newpassword" placeholder="不得小于六位数" required></li>
						 </ul>
						 <input type="submit" value="确定修改" >
						 <div style="margin-top: 25px">
						<div style="margin-top: -430px;margin-left: 480px;width: 280px;">
						 <h4 >更换头像</h4>
						 <c:if test="${user.picture != null}">
						 <a href = "${pageContext.request.contextPath}/download.do?filename=${user.picture}"> 	
						 <img src="	${pageContext.request.contextPath}/download.do?filename=${user.picture}"  height="250px" width="250px"/>
						 </a>							 
						 </c:if>
						 <c:if test="${user.picture == null }">文件不存在</c:if>
				 <p style="padding-top: 10px;">选择本地照片，上传编辑自己的头像</p>
				 <div class="pass-portrait-openimg">
					<input type="file" name="picture" id="" placeholder="${user.picture }">
					 <span style="color: #999;margin-top: 10px;display: block">支持jpg、jpeg、gif、png、bmp格式的图片</span>
				 </div>
			 </div>
			 </div>
		 </form>
				 </div>
			 </div>
			 
		 </div>
	 </div>
		 <div class="col-md-3 sidebar">
			 <h3>个人资料</h3>
			 <ul>
				 <li><a href="${pageContext.request.contextPath}/mycenter.jsp"><span> </span>基本资料</a></li>
				 <li><a href="#"><span> </span>收货地址</a></li>
				 <li><a href="#"><span> </span>其他</a></li>
			 </ul>

			 <h3 class="arch">安全设置</h3>
			 <ul>
				 <li class="active1"><a href="${pageContext.request.contextPath}/Chpassword.jsp"><span> </span>修改登录密码</a></li>
				 <li><a href="${pageContext.request.contextPath}/collect.jsp"><span> </span>商品收藏</a></li>
				 <li><a href="${pageContext.request.contextPath}/order.jsp"><span> </span>我的订单</a></li>
				 <li><a href="#"><span> </span>账号申诉</a></li>
			 </ul>
			 <div class="clear_fix"></div>

		 </div>
		 <div class="clear_fix"> </div>
</div>
</div>
<jsp:include page="footer.jsp"/>