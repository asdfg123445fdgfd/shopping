<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="renderer" content="webkit">
		<meta name="applicable-device" content="pc,mobile">
	<title>findpassword</title>
	<link href="${pageContext.request.contextPath}/assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/allstyle.css" rel="stylesheet" type="text/css" media="all" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/css/component.css" />
	<link href="${pageContext.request.contextPath}/assets/css/menu.css" rel="stylesheet" type="text/css" media="all" />
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/menu.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/jquery.easydropdown.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/simpleCart.min.js"> </script>


	<!-- start menu -->
<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
<!-- start menu -->
</head>
<body>
<jsp:include page="header.jsp"/>
<div class="registration-form">
	 <div class="container">
		 <ol class="breadcrumb">
		  <li><a href="${pageContext.request.contextPath}/index.jsp">首页</a></li>
		  <li class="active">忘记密码</li>
		 </ol>
		 <h2>显示密码</h2>
		 <div class="col-md-6 reg-form">
			 <div class="reg">
				 <p>欢迎，请查看您的密码...</p>
				 <form style="width: 100%;height: 250px">
					 <ul>
						 <li class="text-info1">显示找回密码:</li>
						 <li style="padding-top: 8px">${newInfos.password}</li>
					 </ul>
				 </form>
			 </div>
		 </div>
		 <div class="col-md-6 login-right">
			 <h3>NEW 登录</h3>
			 <p>通过在我们的商店中创建一个帐户，您将能够通过我们的商店创建一个帐户，您将能够更快地通过结帐过程，存储多个送货地址，查看和跟踪您的帐户和更多的订单。</p>

			 <a class="acount-btn" href="${pageContext.request.contextPath}/login.jsp">返回登录</a>

		 </div>
		 <div class="clear_fix"></div>		 
	 </div>
</div>
<jsp:include page="footer.jsp"/>
		