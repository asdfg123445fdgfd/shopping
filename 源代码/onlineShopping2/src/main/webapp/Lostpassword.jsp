<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="renderer" content="webkit">
		<meta name="applicable-device" content="pc,mobile">
	<title>Lostpassword</title>
	<link href="${pageContext.request.contextPath}/assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/allstyle.css" rel="stylesheet" type="text/css" media="all" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/css/component.css" />
	<link href="${pageContext.request.contextPath}/assets/css/menu.css" rel="stylesheet" type="text/css" media="all" />

	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/menu.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/jquery.easydropdown.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/simpleCart.min.js"> </script>


<!-- start menu -->
<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
<!-- start menu -->
</head>
<body>
<jsp:include page="header.jsp"/>
<div class="registration-form">
	 <div class="container">
		 <ol class="breadcrumb">

			 <li><a href="${pageContext.request.contextPath}/index.jsp">首页</a></li>
			 <li class="active">忘记密码</li>

		 </ol>
		 <h2>找回密码</h2>
		 <div class="col-md-6 reg-form">
			 <div class="reg">
				 <p>欢迎，请输入以下内容继续...</p>

				 <p>如果你现在想起密码，<a href="${pageContext.request.contextPath}/login.jsp">点击这里</a></p>

				 <form action="${pageContext.request.contextPath}/Lostpassword.do" method="post"  role="form">
					 <ul>
						 <li class="text-info">手机号码: </li>
						 <li><input type="text" value="" name="telephone" placeholder="输入11位数"></li>
					 </ul>
					<ul>
						 <li class="text-info">用户E-mail: </li>
						 <li><input type="text" value="" name="Email" placeholder="输入11位数" /></li>
					 </ul>
					 <ul>
						 <li class="text-info">用户账号:</li>
						 <li><input type="text" value="" name="usercode" placeholder="不得小于六位数"></li>
					 </ul>
					 <ul>
						 <li class="text-info">验证码:</li>
						 <li style="width: 260px;height: 20px">
						 <input type="password" name="kaptcha" style="float: left" placeholder="输入密码,必须大于6位">
							  
							 <img id="code_img" src="${pageContext.request.contextPath}/kaptcha.png" style="margin-top: -65px;margin-left: 262px;width:132px;height:40px">
						 </li>
					 </ul>
					 <input type="submit" value="立即找回">
					 <p class="click">点击这个按钮，你就会找回<a href="#" style="color: #2aacc8">密码</a> </p>
				 </form>
			 </div>
		 </div>


		 <div class="col-md-6 login-right">
			 <h3>NEW 登录</h3>
			 <p>通过在我们的商店中创建一个帐户，您将能够通过我们的商店创建一个帐户，您将能够更快地通过结帐过程，存储多个送货地址，查看和跟踪您的帐户和更多的订单。</p>

			 <a class="acount-btn" href="${pageContext.request.contextPath}/login.jsp">返回登录</a>

		 </div>
		 <div class="clear_fix"></div>		 
	 </div>
</div>
<jsp:include page="footer.jsp"/>
		