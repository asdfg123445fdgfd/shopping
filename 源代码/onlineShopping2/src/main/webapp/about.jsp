<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="renderer" content="webkit">
		<meta name="applicable-device" content="pc,mobile">

	<title>关于我们</title>
	<link href="${pageContext.request.contextPath}/assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/allstyle.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/component.css" rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/assets/css/menu.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/styles.css" rel="stylesheet"/>

	<script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/simpleCart.min.js"> </script>
	<script src="${pageContext.request.contextPath}/assets/js/menu.js" type="text/javascript" ></script>


<!-- start menu -->
<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>

	<!-- start menu -->
</head>
<body>
<jsp:include page="header.jsp"/>
	<div class="about">
		 <div class="container">
				<ol class="breadcrumb">
		  			<li><a href="${pageContext.request.contextPath}/index.jsp">主页</a></li>
		 			 <li class="active">关于</li>
		 		</ol>
			 <div style="height: 300px;width: 1150px">
		 		<h2>关于我们</h2>
		 		<div class="about-sec">
			 	<div class="about-pic">
				 <img src="${pageContext.request.contextPath}/assets/images/images/a1.jpg" class="img-responsive" alt=""/>
			 </div>
			 	<div class="about-info">
				 <p>这是我所知道的。这是我们最喜欢的。在此基础上，研究了一种不同的方法。有花蜜的花椰菜。
					 马奎里以他的名字命名。在此基础上，研究了一种不同的方法。
					 莫尔比·比本杜姆，普里腾·斯珀，莫尔比斯·尤斯托，他的灵药。Aenean韦尔ipsum苹果。</p>
				 <a href="#">阅读更多…</a>
			 </div>
			</div>
			 </div>
			 <div style="width: 1200px;height: 480px;border: solid 2px #999;margin-top: 30px">
			 	<h3 style="margin-top: 5px;margin-left: 10px">成员介绍</h3>
			 	<div class="about-grids">
					<div class="img">
						<img src="${pageContext.request.contextPath}/assets/images/images/郑绪洪.jpg" class="img_b" alt=""/>
						<a href="#"><h4>项目负责人&郑绪洪</h4></a>
						<p>负者安排工作和实现后端功能</p>
					</div>
					<div class="img">
						<img src="${pageContext.request.contextPath}/assets/images/images/陈中玉_03.png" class="img_b" alt=""/>
						<a href="#"><h4>前段设计&陈中玉</h4></a>
						<p>负者前端设计和后端功能实现</p>
					</div>
					<div class="img">
						<img src="${pageContext.request.contextPath}/assets/images/images/胡熹敏.jpg" class="img_b" alt=""/>
						<a href="#"><h4>后端设计&胡熹敏</h4></a>
						<p>负者后端功能和数据库设计</p>
					</div>
					<div class="img">
						<img src="${pageContext.request.contextPath}/assets/images/images/陈花_03.png" class="img_b" alt=""/>
						<a href="#"><h4>文档编辑&陈花</h4></a>
						<p>负者文档编辑和整合</p>
					</div>
					<div class="img">
						 <img src="${pageContext.request.contextPath}/assets/images/images/张偲.jpg" class="img_b" alt=""/>
						 <a href="#"><h4>项目测试&张偲</h4></a>
						 <p>负者找材料和测试</p>
					</div>
					 <div class="clearfix"></div>
				</div>
			</div>
	 </div>
</div>
<jsp:include page="footer.jsp"/>

