<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="renderer" content="webkit">
		<meta name="applicable-device" content="pc,mobile">
	<title>商品编辑</title>
	<link href="${pageContext.request.contextPath}/assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/allstyle.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/menu.css" rel="stylesheet" type="text/css" media="all" />
	<script src="${pageContext.request.contextPath}/assets/js/menu.js" type="text/javascript" ></script>
	<script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/simpleCart.min.js"> </script>
<!-- start menu -->

<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>

	<!-- start menu -->
</head>
<body>
<jsp:include page="header.jsp"/>
<div class="blog">
	 <div class="container">
		 <ol class="breadcrumb">
		  <li><a href="${pageContext.request.contextPath}/index.jsp">主页</a></li>
		  <li><a href="${pageContext.request.contextPath}/list.do">管理商品</a></li>

		  <li class="active">编辑页面</li>
		 </ol>
	 <h2>卖家编辑商品</h2>
	 <div class="col-md-9 fashion-blogs">

		 <div class="comments-main"  style="width: 1150px;height: 540px">

			 <div class="col-md-6 reg-form" style="float: left">
				 <div class="reg1">
					 <p style="margin-top: 25px">欢迎，请输入以下内容继续...</p>
						 <form action = "${pageContext.request.contextPath}/amend.do" method="post" class="form-horizontal" role="form"  enctype="multipart/form-data">
						 <ul>
							 <li class="text-info">商品编号:</li>
							 <li><input name="serial_num" type="text" readonly="readonly" value="${shop.serial_num}"></li>
						 </ul>
						 <ul>
						 	<li class="text-info">商品名称:</li>
						 	<li><input type="text" name = "trade_name" value="${shop.trade_name}"></li>
						 </ul>
						 
						 
						 <ul>
							 <li class="text-info">商品品牌:</li>
							 <li><input type="text" name = "trade_brand" value="${shop.trade_brand}"></li>
						 </ul>
						 <ul>
							 <li class="text-info">商品邮费: </li>
							 <li><input type="text" name = "trade_carriage" value="${shop.trade_carriage}"></li>
			 			</ul>
						 <ul>
							 <li class="text-info">商品库存: </li>
							 <li><input type="text" name = "trade_num" value="${shop.trade_num}"></li>
						 </ul>
						 <ul>
							 <li class="text-info">商品单价:</li>
							 <li><input type="text" name = "trade_price" value="${shop.trade_price}"></li>
						 </ul>

						 <ul>
							 <li class="text-info">商品类型:</li>
							 <li><input type="text" name = "trade_type" value="${shop.trade_type}"></li>
						 </ul>
						 <ul>
							 <li class="text-info">商品尺码:</li>
							 <li><input type="text" name = "trade_size" value="${shop.trade_size}"></li>
						 </ul>
						 
						 <input type="submit" value="确定修改" style="width:300px;margin-left: 700px;margin-top: -50px">
						<div style="margin-top: 25px">
							<div style="margin-top: -500px;margin-left: 700px;width: 330px;">
						 	<h4 >商品图片</h4>
						 	<img src="${pageContext.request.contextPath}/download.do?filename=${shop.picture}" height="250" width="250"/>
				 	 		<c:if test="${shop.picture != null}">
							 <a href = "${pageContext.request.contextPath}/download.do?filename=${shop.picture}">
							 </a>							 
							 </c:if>
							 <c:if test="${shop.picture == null }"><p>文件不存在</p></c:if>
							 
				 			<p style="padding-top: 10px;">选择本地照片，上传商品图片</p>
							 <div class="pass-portrait-openimg">
					 		<input type="file" name = "picture" id="" value="选择图片">
							<span style="color: #999;margin-top: 10px;display: block">支持jpg、jpeg、gif、png、bmp格式的图片</span>
							 </div>
			 				</div>
			 				</div>						 
				</form>
	 </div>
		 	 </div>
			 </div>
	</div>
		 <div class="clear_fix"> </div>
</div>
</div>
<jsp:include page="footer.jsp"/>