<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="renderer" content="webkit">
		<meta name="applicable-device" content="pc,mobile">

	<title>购物车</title>

	<link href="${pageContext.request.contextPath}/assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/allstyle.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/menu.css" rel="stylesheet" type="text/css" media="all" />

	<script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/simpleCart.min.js"> </script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/menu.js"></script>
<!-- start menu -->
	<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
<!-- start menu -->
</head>
<body>
<jsp:include page="header.jsp"/>
<div class="cart">
	 <div class="container">
			 <ol class="breadcrumb">
		  <li><a href="${pageContext.request.contextPath}/index.jsp">主页</a></li>
		  <li class="active">购物车</li>
		 </ol>
		 <div class="cart-top">
			<a href="${pageContext.request.contextPath}/index.jsp"> 返回主页</a>
		 </div>	
			
		 <div class="col-md-9 cart-items">
			 <h2>我的购物车 (2)</h2>
			 <c:forEach items="${shops}" var="gg">
				<script>$(document).ready(function(c) {
					$('.close1').on('click', function(c){
						$('.cart-header').fadeOut('slow', function(c){
							$('.cart-header').remove();
						});
						});	  
					});
			    </script>
			 <div class="cart-header">
				 <div class="close1"> </div>
				 <div class="cart-sec">
						<div class="cart-item cyc">
							 <img src="${pageContext.request.contextPath}/assets/images/images/pic-2.jpg"/>
						</div>
					   <div class="cart-item-info">
						   <h3>商品名称：<span>${gg.name}</span></h3><h6> 商品编号: <span>${gg.serial_num}</span></h6><h6> 尺码: <span>${gg.trade_size}</span></h6>
							 <h4><span>价格 $ </span>gg.trade_price</h4>
							 <p class="qty">数量 ::</p>
							 <input min="1" type="number" id="num1" name="quantity" value="1" class="form-control input-small">
						     <input min="1" type="hidden">
					   </div>
					   <div class="clearfix"></div>
						<div class="delivery">
							 <p>运费:: <span>${gg.trade_carriage}</span></p>
							 <span>2-3个工作日送货</span>
							 <div class="clearfix"></div>
				        </div>						
				  </div>
			 </div>

			</c:forEach>

			 <script>
				 $(document).ready(function(c) {
					$('.close2').on('click', function(c){
							$('.cart-header2').fadeOut('slow', function(c){
						$('.cart-header2').remove();
					});
					});	  
					});
			 </script>
			 
			 <!--lalalallalallalallaalalalalall-->
			 <div class="cart-header2">
				 <div class="close2"> </div>
				  <div class="cart-sec">
						<div class="cart-item">
							 <img src="${pageContext.request.contextPath}/assets/images/images/pic-1.jpg"/>
						</div>
					   <div class="cart-item-info">
							 <h3>商品名称：<span> nicaicii</span></h3><h6> 商品编号: <span>3578</span></h6>
						     <h6> 尺码: <span>S</span></h6>
							 <h4><span>价格 $ </span>520.00</h4>
							 <p class="qty">数量 ::</p>
							 <input min="1" type="number" id="num2" name="quantity" value="1" class="form-control input-small">
						     <input min="1" type="hidden" >
					   </div>
					   <div class="clearfix"></div>
						<div class="delivery">
							 <p>运费:: <span>$50</span></p>
							 <span>2-3个工作日送货</span>
							 <div class="clearfix"></div>
				        </div>
				  </div>
			  </div>
 
<!--lalalallalallalallaalalalalall-->
		 </div>
		 
		 <div class="col-md-3 cart-total">
			 <a class="continue" href="${pageContext.request.contextPath}/shop.jsp">继续购物</a>
			 <div class="price-details">
				 <h3>价格清单</h3>
				 <span>合计</span>
				 <span class="total">${gg.trade_carriage}</span>
				 <span>折扣</span>
				 <span class="total">---</span>
				 <span>运费</span>
				 <span class="total">100.00</span>
				 <div class="clearfix"></div>				 
			 </div>	
			 <h4 class="last-price">总价</h4>
			 <span class="total final">450.00</span>
			 <div class="clearfix"></div>
			 <div class="pay">
				 <p>支付方式：</p>
				 <img src="${pageContext.request.contextPath}/assets/images/images/QQ.png" onmouseover="this.src='${pageContext.request.contextPath}/assets/images/images/QQ (red).png'" onmouseout="this.src='${pageContext.request.contextPath}/assets/images/images/QQ.png'" height="50" width="50"/>
				 <img src="${pageContext.request.contextPath}/assets/images/images/微信 (blue).png" onmouseover="this.src='${pageContext.request.contextPath}/assets/images/images/微信.png'" onmouseout="this.src='${pageContext.request.contextPath}/assets/images/images/微信 (blue).png'"height="50" width="50"/>
				 <img src="${pageContext.request.contextPath}/assets/images/images/支付宝.png" onmouseover="this.src='${pageContext.request.contextPath}/assets/images/images/支付宝 (1).png'" onmouseout="this.src='${pageContext.request.contextPath}/assets/images/images/支付宝.png'" height="50" width="50"/>

			 </div>
			 <a class="order" href="#">立即下单</a>
			 <div class="total-item">
				 <h3>选择</h3>
				 <h4>优惠券</h4>
				 <a class="cpns" href="#">使用优惠券</a>
				 <p><a href="${pageContext.request.contextPath}/mycenter.jsp">收货地址</a> 重庆工程学院</p>
			 </div>
			</div>
	 </div>
</div>
<jsp:include page="footer.jsp"/>

