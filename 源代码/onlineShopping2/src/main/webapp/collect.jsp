<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="renderer" content="webkit">
		<meta name="applicable-device" content="pc,mobile">
	<title>商品收藏</title>
	<link href="${pageContext.request.contextPath}/assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/allstyle.css" rel="stylesheet" type="text/css" media="all" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/css/component.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/styles.css">
	<link href="${pageContext.request.contextPath}/assets/css/menu.css" rel="stylesheet" type="text/css" media="all" />
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/menu.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/simpleCart.min.js"> </script>
<!-- start menu -->

<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>

	<!-- start menu -->
</head>
<body>
<jsp:include page="header.jsp"/>
<div class="blog">
	 <div class="container">
		 <ol class="breadcrumb">
		   <li><a href="${pageContext.request.contextPath}/index.jsp">主页</a></li>
			 <li><a href="${pageContext.request.contextPath}/mycenter.jsp">个人中心</a></li>
			 <li class="active">商品收藏</li>
		 </ol>
	 <h2>商品收藏</h2>
         <div class="col-md-9 fashion-blogs">
             <div class="comments-main" style="height: 550px">
				 <div id="vipRight">
					 <div class="hidden_yh bj_fff" style="width:835px;">
						 <div class="width100 font24" style="height:60px;line-height:60px;text-indent:50px;background:#f5f8fa;border-bottom:1px solid #ddd;">最近收藏</div>
						 <div class="hidden_yh" style="padding:20px;width:835px;">
							 <!--一条-->
							 <div class="width100 hidden_yh" style="border-bottom:1px dashed #ddd;padding-top:15px;padding-bottom:15px;">
								 <img src="${pageContext.request.contextPath}/assets/images/images/a1.jpg" width="100" height="100" class="left_yh">
								 <div class="left_yh" style="width:580px;">
									 <h3 class="font18 c_33 font100" style="margin-left: 25px">2017玉石摆件客厅装饰品玄关柜招财工艺品摆设</h3>
									 <p class="red collect" style="margin-top:10px;">单价::￥268.00</p>
								 </div>
								 <div class="right_yh">
									 <a href="javascript:void(0)" class="off block_yh tcenter font16" style="margin-top:10px;padding:6px;">加入购物车</a>
									 <a href="javascript:void(0)" class="off block_yh tcenter font16" style="margin-top:10px;padding:6px;">取消收藏</a>
								 </div>
							 </div>
							 <!--一条-->
							 <div class="width100 hidden_yh" style="border-bottom:1px dashed #ddd;padding-top:15px;padding-bottom:15px;">
								 <img src="${pageContext.request.contextPath}/assets/images/images/a1.jpg" width="100" height="100" class="left_yh">
								 <div class="left_yh" style="width:580px;">
									 <h3 class="font18 c_33 font100" style="margin-left: 25px">2017玉石摆件客厅装饰品玄关柜招财工艺品摆设</h3>
									 <p class="red collect" style="margin-top:10px;">单价::￥268.00</p>
								 </div>
								 <div class="right_yh">
									 <a href="javascript:void(0)" class="off block_yh tcenter font16" style="margin-top:10px;padding:6px;">加入购物车</a>
									 <a href="javascript:void(0)" class="off block_yh tcenter font16" style="margin-top:10px;padding:6px;">取消收藏</a>
								 </div>
							 </div>
							 <!--一条-->
							 <div class="width100 hidden_yh" style="border-bottom:1px dashed #ddd;padding-top:15px;padding-bottom:15px;">
								 <img src="${pageContext.request.contextPath}/assets/images/images/a1.jpg" width="100" height="100" class="left_yh">
								 <div class="left_yh" style="width:580px;">
									 <h3 class="font18 c_33 font100" style="margin-left: 25px">2017玉石摆件客厅装饰品玄关柜招财工艺品摆设</h3>
									 <p class="red collect" style="margin-top:10px;">单价::￥268.00</p>
								 </div>
								 <div class="right_yh">
									 <a href="javascript:void(0)" class="off block_yh tcenter font16" style="margin-top:10px;padding:6px;">加入购物车</a>
									 <a href="javascript:void(0)" class="off block_yh tcenter font16" style="margin-top:10px;padding:6px;">取消收藏</a>
								 </div>
							 </div>
						 </div>
						 <ul class="pagination pull-right" style="margin-top: 0px;">
							 <li><a href="#">&laquo;</a></li>
							 <li><a href="#">1</a></li>
							 <li><a href="#">2</a></li>
							 <li><a href="#">3</a></li>
							 <li><a href="#">4</a></li>
							 <li><a href="#">5</a></li>
							 <li><a href="#">&raquo;</a></li>
						 </ul>
					 </div>
				 </div>

             </div>
         </div>
	 <div class="col-md-3 sidebar">
		 <h3>个人资料</h3>
		 <ul>
			 <li><a href="${pageContext.request.contextPath}/mycenter.jsp"><span> </span>基本资料</a></li>
			 <li><a href="#"><span> </span>收货地址</a></li>
			 <li><a href="#"><span> </span>其他</a></li>
		 </ul>

		 <h3 class="arch">安全设置</h3>
		 <ul>
			 <li><a href="${pageContext.request.contextPath}/Chpassword.jsp"><span> </span>修改登录密码</a></li>
			 <li><a href="${pageContext.request.contextPath}/collect.jsp"><span> </span>商品收藏</a></li>
			 <li><a href="${pageContext.request.contextPath}/order.jsp"><span> </span>我的订单</a></li>
			 <li><a href="#"><span> </span>账号申诉</a></li>
		 </ul>
		<div class="clear_fix"></div>

     </div>
	 <div class="clear_fix"> </div>
</div>
</div>
<jsp:include page="footer.jsp"/>