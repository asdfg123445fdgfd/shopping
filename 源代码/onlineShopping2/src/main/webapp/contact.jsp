<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="renderer" content="webkit">
		<meta name="applicable-device" content="pc,mobile">
	<title>Contact</title>
	<link href="${pageContext.request.contextPath}/assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/allstyle.css" rel="stylesheet" type="text/css" media="all" />

	<link href="${pageContext.request.contextPath}/assets/css/menu.css" rel="stylesheet" type="text/css" media="all" />
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/menu.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/simpleCart.min.js"> </script>
<!-- start menu -->
<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
<!-- start menu -->
</head>
<body>
<jsp:include page="header.jsp"/>
<div class="contact-section-page">
	   <div class="contact_top">					
		   <div class="container">
				<ol class="breadcrumb">
		  <li><a href="${pageContext.request.contextPath}/index.jsp">主页</a></li>
		  <li class="active">联系我们</li>
		 </ol>
			  <div class="col-md-6 contact_left">
			 		<h2>联系表单</h2>
			 		<p>人际关系是人与人之间的沟通，是用现代方式表达出圣经中“欲人施于己者，必先施于人”的金科玉律</p>
				  <form>
					 <div class="form_details">
						   <input type="text" class="text" value="Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}"/>
						   <input type="text" class="text" value="Email Address" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email Address';}"/>
							<input type="text" class="text" value="Title" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Title';}"/>
							<textarea value="Content" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Content';}">Content</textarea>
							<div class="clear_fix"> </div>					   
							<input name="submit" type="submit" value="Send Message">
					 </div>					 
				  </form>
			 </div>
			 <div class="col-md-6 company-right">
					<div class="contact-map">
						<img src="${pageContext.request.contextPath}/assets/images/images/color.jpg" height="285" width="546"/>
					</div>
				 <div class="company-right">
					   <div class="company_ad">
							<h3>联系资料</h3>
							<span>他们坐在一起，看着他们。</span>
			      			<address>
								<p>E-mail:<a href="#">info@display.com</a></p>
								 <p>电话:  +5201314520</p>
								<p>28-7-169,第二大街南</p>
								<p>28-7-169，第2个萨卡布什，SK S7M，南大街1号</p>
							</address>
					  </div>
				 </div>							
			 </div>
		  </div>
	 </div>
</div>
<jsp:include page="footer.jsp"/>