<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!--fotter-->

<div class="fotter-logo" style="background: #fa558f;padding: 1.5em 0;c">
	 <div class="container">

		 <div class="ftr-logo"><h3><a href="${pageContext.request.contextPath}/index.jsp" style="font-family: 'Raleway', sans-serif;font-size: 1em;font-weight: 700;color: #fff;text-decoration: none;">新时尚，新潮流</a></h3></div>

		 <div class="ftr-info">
			 <p style="color: #fff;font-weight: 400;font-size: 1.2em;
    margin-top: 1em;">© 2005-2020  梅茜茜 版权所有，并保留所有权利。<a href="#" target="_blank" title="ICP">ICP备案证书号：粤ICP备08008334号</a></p>
		 </div>
	 <div class="clearfix"></div>
	 </div>
</div>
<!--fotter//-->	
</body>
</html>