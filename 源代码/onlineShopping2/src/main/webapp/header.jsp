<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/jsp; charset=UTF-8">
<title>头部</title>
</head>
<body>


<!--header-->


<div class="header2 text-center">

	<div class="container">
		<div class="main-header">
		<div class="carting">
		<c:if test="${user==null}">
		<h4><a href="${pageContext.request.contextPath}/login.jsp" style="color: #fff;float: left">登录</a></h4>
				</c:if>
				<c:if test="${user!=null}">
				<h4><a href="${pageContext.request.contextPath}/user/logout.do" style="color: #fff;float: left">退出</a></h4>
				</c:if>
                <span style="color: #999;display: block;margin-left: 50px">欢迎:
                    <span style="color: #2aacc8;font-weight:bold;">${user.username}</span>
                </span>
			</div>
			<div class="logo">
				<h3><a href="${pageContext.request.contextPath}/index.jsp">NEW FASHIONS</a></h3>
			</div>
			<div class="box_1">
				<a href="${pageContext.request.contextPath}/shop/cart.do"><h3>购物车:<img src="${pageContext.request.contextPath}/assets/images/cart.png" alt=""/></h3></a>
			</div>
			<div class="clear_fix"></div>
		</div>

		<!-- start header menu -->
		<ul class="megamenu skyblue" id="navUl" style="margin-top: 20px">
			<li class="active grid"><a class="color1" href="index.jsp">主页</a>
			<li ><a href="#">在线商城</a>
				<ul class="subNav">
					<li><a href="${pageContext.request.contextPath}/shop/List.do"><h4>店铺</h4></a>
					<li><a href="${pageContext.request.contextPath}/products.jsp"><h4>女装</h4></a>
					<li><a href="${pageContext.request.contextPath}/men.jsp"><h4>男装</h4></a>
					<li><a href="${pageContext.request.contextPath}/child.jsp"><h4>童装</h4></a>
					<li><a href="${pageContext.request.contextPath}/shop.jsp"><h4>其他</h4></a>
				</ul>
			<li><a href="${pageContext.request.contextPath}/recommend.jsp">今日推荐</a>
			<li><a href="${pageContext.request.contextPath}/contact.jsp">联系我们</a>
			<li class="grid"><a href="${pageContext.request.contextPath}/about.jsp">关于我们</a>
			<li class="grid"><a href="${pageContext.request.contextPath}/mycenter.jsp">个人中心</a>
		</ul>
		<div class="clear_fix"></div>
	</div>


			 
	 </div>

<script>
    (function(){

        var  navUl = document.getElementById("navUl");
        var  lis = navUl.getElementsByTagName("li");

        var  i = 0;  // 下标（索引）从0开始
        for( i=0;  i<= lis.length-1; i++ ){
            lis[i].onmouseover = function(){
                var erji = this.getElementsByTagName("ul");

                if( erji.length == 0 ){
                    return null; // 终止函数运行
                }

                erji[0].style.display="block";
            }
            lis[i].onmouseout = function(){
                var erji = this.getElementsByTagName("ul");
                if( erji.length == 0 ){
                    return null; // 终止函数运行
                }

                erji[0].style.display="none";
            }
        }

    })();
</script>

<!--header//-->

</body>
</html>