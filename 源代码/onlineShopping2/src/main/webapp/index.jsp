<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="renderer" content="webkit">
		<meta name="applicable-device" content="pc,mobile">
	<title>时尚时装秀购物</title>
	<link href="${pageContext.request.contextPath}/assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/allstyle.css" rel="stylesheet" type="text/css" media="all" />

	<link href="${pageContext.request.contextPath}/assets/css/component.css" rel="stylesheet" type="text/css" />

	<link href="${pageContext.request.contextPath}/assets/css/menu.css" rel="stylesheet" type="text/css" media="all" />
	<script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/simpleCart.min.js"> </script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/menu.js"></script>

<!-- start menu -->


<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>

	<!-- start menu -->
</head>
<!--header-->
<div class="header">
	<div class="container">
		<div class="main-header">
		<div class="carting">
		<c:if test="${user==null}">
		<h4><a href="${pageContext.request.contextPath}/login.jsp" style="color: #fff;float: left">登录</a></h4>
				</c:if>
				<c:if test="${user!=null}">
				<h4><a href="${pageContext.request.contextPath}/user/logout.do" style="color: #fff;float: left">退出</a></h4>
				</c:if>
                <span style="color: #999;display: block;margin-left: 50px">欢迎
                    <span style="color: #2aacc8;font-weight:bold;">${user.username}</span></span>
			</div>
			<div class="logo">
				<h3><a href="index.jsp">NEW FASHIONS</a></h3>
			</div>
			<div class="box_1">
				<a href="cart.jsp"><h3>购物车:<img src="${pageContext.request.contextPath}/assets/images/cart.png" alt=""/></h3></a>
			</div>
			<div class="clear_fix"></div>
		</div>

		<!-- start header menu -->
		<ul class="megamenu skyblue" id="navUl" style="margin-top: 20px">
			<li class="active grid"><a class="color1" href="index.jsp">主页</a>
			<li ><a href="#">在线商城</a>
				<ul class="subNav">
					<li><a href="shop.jsp"><h4>店铺</h4></a>
					<li><a href="products.jsp"><h4>女装</h4></a>
					<li><a href="men.jsp"><h4>男装</h4></a>
					<li><a href="child.jsp"><h4>童装</h4></a>
					<li><a href="shop.jsp"><h4>其他</h4></a>
				</ul>
			<li><a href="${pageContext.request.contextPath}/shop/List.do">今日推荐</a>
			<li><a href="contact.jsp">联系我们</a>
			<li class="grid"><a href="about.jsp">关于我们</a>
			<li class="grid"><a href="mycenter.jsp">个人中心</a>
		</ul>
		<div class="clear_fix"></div>
	</div>
		 <div class="caption">
		 <h1>FASHION AND CREATIVITY</h1>	 
		 <p>Sed dapibus est a lorem dictum, id dignissim lacus fermentum. Nulla ut nibh in libero maximus pretium
		 Nunc vulputate vel tellus ac elementum. Duis nec tincidunt dolor, ac dictum eros.</p>
	     </div>
</div>
<script>
    (function(){

        var  navUl = document.getElementById("navUl");
        var  lis = navUl.getElementsByTagName("li");

        var  i = 0;  // 下标（索引）从0开始
        for( i=0;  i<= lis.length-1; i++ ){
            lis[i].onmouseover = function(){
                var erji = this.getElementsByTagName("ul");

                if( erji.length == 0 ){
                    return null; // 终止函数运行
                }

                erji[0].style.display="block";
            }
            lis[i].onmouseout = function(){
                var erji = this.getElementsByTagName("ul");
                if( erji.length == 0 ){
                    return null; // 终止函数运行
                }

                erji[0].style.display="none";
            }
        }

    })();
</script>
<script>
    (function(){

        var  navUl = document.getElementById("navUl");
        var  lis = navUl.getElementsByTagName("li");

        var  i = 0;  // 下标（索引）从0开始
        for( i=0;  i<= lis.length-1; i++ ){
            lis[i].onmouseover = function(){
                var erji = this.getElementsByTagName("ul");

                if( erji.length == 0 ){
                    return null; // 终止函数运行
                }

                erji[0].style.display="block";
            }
            lis[i].onmouseout = function(){
                var erji = this.getElementsByTagName("ul");
                if( erji.length == 0 ){
                    return null; // 终止函数运行
                }

                erji[0].style.display="none";
            }
        }

    })();
</script>
<!--header-->
<div class="categories">
	 <div class="container">
		 <a href="#"><div class="col-md-4 sections fashion-grid-a">
			 <h4>Fashion</h4>
			 <p>dignissim</p>			 					
		 </div></a>
		 <a href="#"><div class="col-md-4 sections fashion-grid-b">
			 <h4>Beauty</h4>
			 <p>fermentum</p>			 					
		 </div></a>
		 <a href="#"><div class="col-md-4 sections fashion-grid-c">
			 <h4>Creativity</h4>
			 <p>vulputate</p>				
		 </div></a>
	 </div>
</div>
<!---->
	<div class="video">
		<div style="width: 1139px;margin: auto;">
			<video controls="controls" autoplay="autoplay" preload="auto" width="100%">
				<source src="${pageContext.request.contextPath}/assets/video/505315289656853657779.mp4" type="video/mp4">
			</video>
		</div>
	</div>
<!--foot-->
<div class="foot"  style="margin-top: 10px">
	 <div class="container">
	 <div class="col-md-6 contact">
		  <form>
			 <input type="text" class="text" value="Name..." onFocus="this.value = '';" onBlur="if (this.value == '') {this.value = 'Name...';}">
			 <input type="text" class="text" value="Email..." onFocus="this.value = '';" onBlur="if (this.value == '') {this.value = 'Email...';}">
			 <textarea onFocus="if(this.value == 'Message...') this.value='';" onBlur="if(this.value == '') this.value='Message...';" >Message...</textarea>	
			 <div class="clear_fix"></div>
			 <input type="submit" value="提交">
		 </form>

	 </div>
	 <div class="col-md-6 ftr-left">
		 <div class="ftr-list">
			 <ul>
				 <li><a href="#">主页</a></li>
				 <li><a href="${pageContext.request.contextPath}/about.jsp">关于</a></li>
				 <li><a href="${pageContext.request.contextPath}/mycenter.jsp">个人中心</a></li>
				 <li><a href="${pageContext.request.contextPath}/products.jsp">最畅销</a></li>
				 <li><a href="${pageContext.request.contextPath}/shop.jsp">新模式</a></li>
				 <li><a href="#">功能箱</a></li>
				 <li><a href="${pageContext.request.contextPath}/products.jsp">接触</a></li>
			 </ul>
		 </div>
		 <div class="ftr-list2">
			 <ul>
				 <li><a href="#">版权</a></li>
				 <li><a href="#">新潮</a></li>
				 <li><a href="#">时尚</a></li>
				 <li><a href="#">学院</a></li>
				 <li><a href="#">公司</a></li>
				 <li><a href="#">追随我们</a></li>
			 </ul>
		 </div>
		 <div class="clear_fix"></div>
		 <h4>合作伙伴</h4>
		 <div class="social-icons">
		 <a href="#"><span class="in"> </span></a>
		 <a href="#"><span class="you"> </span></a>
		 <a href="#"><span class="be"> </span></a>
		 <a href="#"><span class="twt"> </span></a>
		 <a href="#"><span class="fb"> </span></a>
		 </div>
	 </div>	 
	 <div class="clear_fix"></div>
	 </div>
</div>
<jsp:include page="footer.jsp"/>