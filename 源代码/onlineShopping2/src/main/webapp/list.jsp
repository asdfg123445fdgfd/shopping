<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="renderer" content="webkit">
		<meta name="applicable-device" content="pc,mobile">
	<title>list</title>
	<link href="${pageContext.request.contextPath}/assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/allstyle.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/menu.css" rel="stylesheet" type="text/css" media="all" />
	<script type="${pageContext.request.contextPath}/text/javascript" src="assets/js/menu.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/simpleCart.min.js"> </script>
<!-- start menu -->

<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>

	<!-- start menu -->
</head>
<body>
<jsp:include page="header.jsp"/>
<div class="blog">
	 <div class="container" >
		 <ol class="breadcrumb">
		  <li><a href="${pageContext.request.contextPath}/index.jsp">主页</a></li>
		  <li class="active" >
		  	<a href="${pageContext.request.contextPath}/list.do">管理商品</a>
		  </li>
		 </ol>
	<div style="width: 1160px;height: 560px;border: solid 2px #ff8b39">
	 <h2 style="margin-top: 10px;margin-left: 10px">卖家管理商品</h2>
	<div class="container" style="padding-top:15px">
			 
		<form method="post" action="${pageContext.request.contextPath}/list.do" class="navbar-form" style="padding:10px 5px" role="search">
			
			<input type = "hidden" name = "id" value = ""/>
				<div class="form-group">
				 <label  for="trade_name">商品名称</label>
				 <input type="text" name="trade_name" class="form-control" placeholder="支持模糊查询">
					 </div>
					 <div class="form-group">
						 <label for="trade_type">商品类型</label>
						 <input type="text" name="trade_type" class="form-control" placeholder="支持模糊查询">
					 </div>
					 <p class="pull-right" style="margin-right: 10px;margin-bottom:5px">
						 <button type="submit" class="btn btn-primary">查询</button>
						 <a class="btn btn-primary" href="${pageContext.request.contextPath}/edit.do" role="button">新增</a>
					 </p>
		
		<table class="table table-bordered table-striped" style="margin-bottom: 0px;width: 1140px">
			<thead >
				<tr style="magin-top:10px">
					 <th style="width: 50px">序号</th>
					 <th style="width: 100px">商品名称</th>
					 <th>商品编号</th>
					 <th>商品品牌</th>
					 <th>商品邮费</th>
					 <th>商品库存</th>
					 <th>商品单价</th>
					 <th>商品类型</th>
					 <th>商品尺码</th>
					 <th style="width: 150px">商品图片</th>
					 <th style="width:135px">操作</th>
				 </tr>
		</thead>
	<tbody>
				<c:forEach items="${shopp}" var="shopp">
				<tr>
					<td>${shopp.id}</td>
					<td>${shopp.trade_name}</td>
					<td>${shopp.serial_num}</td>
					<td>${shopp.trade_brand}</td>
					<td>${shopp.trade_carriage}</td>
					<td>${shopp.trade_num}</td>
					<td>${shopp.trade_price}</td>
					<td>${shopp.trade_type}</td>
					<td>${shopp.trade_size}</td>
					<td>
					<c:if test="${shopp.picture!=null}">
						<a href="${pageContext.request.contextPath}/download.do?filename=${shopp.picture}">
						<img src="${pageContext.request.contextPath}/download.do?filename=${shopp.picture}" style="height: 50px;width:100px;">
						</a>
					</c:if>
					<c:if test="${shopp.picture==null}">文件不存在</c:if>
					</td>
					<td class="text-center">
						<a class="btn btn-default" href="${pageContext.request.contextPath}/shop/add.do?id=${shopp.id}" role="button">编辑</a>
						<a class="btn btn-default" href="${pageContext.request.contextPath}/delete.do?id=${shopp.id}" role="button">删除</a>
					</td>
				</tr>
				</c:forEach>
	</tbody>				
</table>
			 <c:if test="${shopp.size()>0}">
		<ul class="pagination pull-right" style="margin-top: 10px;">
			<li><a href="${pageContext.request.contextPath}/list.do?currentPage=${pager.prevPage}">&laquo;</a></li>
			<c:choose>
			<c:when test="${pager.totalPage<5}">
				<c:forEach begin="1" end="${pager.totalPage}" var="i">
				<li class="${pager.currentPage==i?'active':'' }">
					<a href="${pageContext.request.contextPath}/list.do?currentPage=${i}">${i}</a>
				</li>
				</c:forEach>
			</c:when>
			<c:when test="${pager.totalPage>=5}">
				<c:forEach begin="0" end="4" var="j">
				<c:if test="${pager.currentPage==pager.totalPage-j}">
					<c:forEach begin="${pager.currentPage-4+j}" end="${pager.currentPage+j}" var="i">
					<li class="${pager.currentPage==i?'active':'' }">
						<a href="${pageContext.request.contextPath}/list.do?currentPage=${i}">${i}</a>
					</li>
					</c:forEach>
				</c:if>
				</c:forEach>
				<c:if test="${pager.currentPage<pager.totalPage-4}">
					<c:forEach begin="${pager.currentPage}" end="${pager.currentPage+4}" var="i">
					<li class="${pager.currentPage==i?'active':'' }">
						<a href="${pageContext.request.contextPath}/list.do?currentPage=${i}">${i}</a>
					</li>
					</c:forEach>
				</c:if>
			</c:when>
			</c:choose>
		    <li><a href="${pageContext.request.contextPath}/list.do?currentPage=${pager.nextPage}">&raquo;</a></li>
		</ul>
		</c:if>
	</form>
	</div>
	</div>
		 <div class="clear_fix"> </div>
</div>
</div>
<jsp:include page="footer.jsp"/>