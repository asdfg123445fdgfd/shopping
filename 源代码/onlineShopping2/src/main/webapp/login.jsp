<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="renderer" content="webkit">
		<meta name="applicable-device" content="pc,mobile">
	<title>Login</title>
	<link href="${pageContext.request.contextPath}/assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/allstyle.css" rel="stylesheet" type="text/css" media="all" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/css/component.css" />
	<link href="${pageContext.request.contextPath}/assets/css/menu.css" rel="stylesheet" type="text/css" media="all" />


	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/menu.js"></script>

	<script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/simpleCart.min.js"> </script>

<!-- start menu -->
<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
<!-- start menu -->
</head>
<body>
<jsp:include page="header.jsp"/>
<div class="login" style="magin-top:-200px">
	 <div class="container">
			<ol class="breadcrumb">
				<li><a href="${pageContext.request.contextPath}/index.jsp">主页</a></li>
				<li class="active">登录</li>
				<li class="active1"><a href="${pageContext.request.contextPath}/admin.jsp">卖家登录</a></li>
				<li class="active1"><a href="${pageContext.request.contextPath}/Lostpassword.jsp">忘记密码</a></li>
				<li class="active1"><a href="${pageContext.request.contextPath}/registration.jsp">注册</a></li>
		 </ol>
		 <h2>登录</h2>
		 <div class="col-md-6 log">			 
				 <p>欢迎，请输入以下内容继续...</p>
				 <p>如果您之前登录过我们，<a href="${pageContext.request.contextPath}/index.jsp"style="margin-top: -28px;margin-left: 175px">点击这里</a></p>
				 <form action="${pageContext.request.contextPath}/login.do" method="post" class="form-horizontal" role="form">
					 <h5>用户账号:</h5>
					 <input type="text" value="" name="usercode" placeholder="不得小于六位数" required>
					 <h5>用户密码:</h5>
					 <input type="password" value="" name="password" placeholder="不得小于六位数" required>					
					 <input type="submit" value="登录" style="margin-left: 130px">
					  <a href="${pageContext.request.contextPath}/Lostpassword.jsp">忘记密码?</a>
				 </form>				 
		 </div>
		  <div class="col-md-6 login-right">
			  	<h3>NEW 注册</h3>
				<p>通过在我们的商店中创建一个帐户，您将能够通过我们的商店创建一个帐户，您将能够更快地通过结帐过程，存储多个送货地址，查看和跟踪您的帐户和更多的订单。</p>
				<a class="acount-btn" href="${pageContext.request.contextPath}/registration.jsp">申请一个新帐号</a>
		 </div>
		 <div class="clear_fix"></div>		 
		 
	 </div>
</div>
<jsp:include page="footer.jsp"/>
		