<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>jspml lang="zh-CN">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="renderer" content="webkit">
		<meta name="applicable-device" content="pc,mobile">
	<title>men</title>
	<link href="${pageContext.request.contextPath}/assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/form.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/allstyle.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/menu.css" rel="stylesheet" type="text/css" media="all" />
	<link rel="stylesheet" type="text/css" href="assets/css/component.css" />

	<script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/jquery.easydropdown.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/simpleCart.min.js"> </script>

	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/menu.js"></script>
<!-- start menu -->
<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
<!-- start menu -->
</head>
<body>
<!--header-->
<div class="header2 text-center">
	 <div class="container">
		 <div class="main-header">
			  <div class="carting">

				  <h4><a href="${pageContext.request.contextPath}/login.jsp" style="color: #fff;float: left">登录</a></h4>

				  <span style="color: #999;display: block;margin-left: 50px">欢迎
					  <span style="color: #2aacc8;font-weight:bold;">用户名称</span></span>
			  </div>
			 <div class="logo">

				 <h3><a href="${pageContext.request.contextPath}/index.jsp">NEW FASHIONS</a></h3>
			  </div>			  
			 <div class="box_1">				 
				 <a href="${pageContext.request.contextPath}/cart.jsp"><h3>购物车:<img src="${pageContext.request.contextPath}/assets/images/images/cart.png" alt=""/></h3></a>

			 </div>
			 
			 <div class="clearfix"></div>
		 </div>		 
				<!-- start header menu -->
		 <ul class="megamenu skyblue" id="navUl"style="margin-top: 20px">

			<li class="grid"><a class="color1" href="${pageContext.request.contextPath}/index.jsp">主页</a></li>
			<li class="active"><a href="${pageContext.request.contextPath}/shop.jsp">在线商城</a>

				<ul class="subNav">
					<li><a href="${pageContext.request.contextPath}/shop.jsp"><h4>店铺</h4></a></li>
					<li><a href="${pageContext.request.contextPath}/products.jsp"><h4>女装</h4></a></li>
					<li><a href="${pageContext.request.contextPath}/men.jsp"><h4>男装</h4></a></li>
					<li><a href="${pageContext.request.contextPath}/child.jsp"><h4>童装</h4></a></li>
					<li><a href="${pageContext.request.contextPath}/shop.jsp"><h4>其他</h4></a></li>
				</ul></li>
			<li><a href="${pageContext.request.contextPath}/recommend.jsp">今日推荐</a></li>
			<li><a href="${pageContext.request.contextPath}/contact.jsp">联系我们</a></li>
			<li class="grid"><a href="${pageContext.request.contextPath}/about.jsp">关于我们</a></li>
			<li class="grid"><a href="${pageContext.request.contextPath}/mycenter.jsp">个人中心</a></li>

		 </ul>
			  <div class="clearfix"></div>
	 </div>
</div>
<script>
    (function(){

        var  navUl = document.getElementById("navUl");
        var  lis = navUl.getElementsByTagName("li");

        var  i = 0;  // 下标（索引）从0开始
        for( i=0;  i<= lis.length-1; i++ ){
            lis[i].onmouseover = function(){
                var erji = this.getElementsByTagName("ul");

                if( erji.length == 0 ){
                    return null; // 终止函数运行
                }

                erji[0].style.display="block";
            }
            lis[i].onmouseout = function(){
                var erji = this.getElementsByTagName("ul");
                if( erji.length == 0 ){
                    return null; // 终止函数运行
                }

                erji[0].style.display="none";
            }
        }

    })();
</script>
<!--header//-->
<div class="product-model">
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="${pageContext.request.contextPath}/index.jsp">主页</a></li>
			<li class="active">男装</li>
		</ol>
		<h2>我们的产品</h2>
		<div class="col-md-9 product-model-sec">
			<div class="product-grid love-grid">
				<a href="${pageContext.request.contextPath}/single.jsp">
					<div class="more-product"><span> </span></div>
					<div class="product-img b-link-stripe b-animate-go  thickbox">
						<img src="${pageContext.request.contextPath}/assets/men/men2.jpg" class="img-responsive" alt=""/>
						<div class="b-wrapper">
							<h4 class="b-animate b-from-left  b-delay03">
								<button class="btns">查看商品详情</button></h4>
						</div>
					</div>
				</a>
					<div class="product-info simpleCart_shelfItem">
						<div class="product-info-cust prt_name">
							<h4>衣服</h4>
							<span class="item_price">单价::￥187.95</span>
							<input type="text" class="item_quantity" value="1" />
							<a href="${pageContext.request.contextPath}/cart.jsp">
								<input type="button" class="item_add items" value="添加购物车">
							</a>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>

			<div class="product-grid love-grid">
					<a href="${pageContext.request.contextPath}/single.jsp">
					<div class="more-product"><span> </span>
					</div>
					<div class="product-img b-link-stripe b-animate-go  thickbox">
						<img src="${pageContext.request.contextPath}/assets/men/men1.jpg" class="img-responsive" alt=""/>
						<div class="b-wrapper">
							<h4 class="b-animate b-from-left  b-delay03">
								<button class="btns">查看商品详情</button>
							</h4>
						</div>
					</div>
				</a>
					<div class="product-info simpleCart_shelfItem">
						<div class="product-info-cust">
							<h4>衣服</h4>
							<span class="item_price">单价::￥187.95</span>
							<input type="text" class="item_quantity" value="1" />
							<a href="${pageContext.request.contextPath}/cart.jsp">
								<input type="button" class="item_add items" value="添加购物车">
							</a>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
			<div class="product-grid love-grid">
				<a href="${pageContext.request.contextPath}/single.jsp">
					<div class="more-product"><span> </span>
					</div>
					<div class="product-img b-link-stripe b-animate-go  thickbox">
						<img src="${pageContext.request.contextPath}/assets/men/men4.jpg" class="img-responsive" alt=""/>
						<div class="b-wrapper">
							<h4 class="b-animate b-from-left  b-delay03">
								<button class="btns">查看商品详情</button>
							</h4>
						</div>
					</div>
				</a>
					<div class="product-info simpleCart_shelfItem">
						<div class="product-info-cust">
							<h4>衣服</h4>
							<span class="item_price">单价::￥187.95</span>
							<input type="text" class="item_quantity" value="1" />
							<a href="${pageContext.request.contextPath}/cart.jsp">
								<input type="button" class="item_add items" value="添加购物车">
							</a>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
			<div class="product-grid love-grid">
				<a href="${pageContext.request.contextPath}/single.jsp">
					<div class="more-product"><span> </span>
					</div>
					<div class="product-img b-link-stripe b-animate-go  thickbox">
						<img src="${pageContext.request.contextPath}/assets/men/men8.jpg" class="img-responsive" alt=""/>
						<div class="b-wrapper">
							<h4 class="b-animate b-from-left  b-delay03">
								<button class="btns">查看商品详情</button>
							</h4>
						</div>
					</div>
				</a>
					<div class="product-info simpleCart_shelfItem">
						<div class="product-info-cust">
							<h4>新棉</h4>
							<span class="item_price">单价::￥187.95</span>
							<input type="text" class="item_quantity" value="1" />
							<a href="${pageContext.request.contextPath}/cart.jsp">
							<input type="button" class="item_add items" value="添加购物车">
						</a>
						</div>
						<div class="clearfix">
						</div>
					</div>
				</div>
			<div class="product-grid love-grid">
				<a href="${pageContext.request.contextPath}/single.jsp">
					<div class="more-product"><span> </span></div>
					<div class="product-img b-link-stripe b-animate-go  thickbox">
						<img src="${pageContext.request.contextPath}/assets/men/men9.jpg" class="img-responsive" alt=""/>
						<div class="b-wrapper">
							<h4 class="b-animate b-from-left  b-delay03">
								<button class="btns">查看商品详情</button>
							</h4>
						</div>
					</div>
				</a>
					<div class="product-info simpleCart_shelfItem">
						<div class="product-info-cust">
							<h4>女装</h4>
							<span class="item_price">单价::￥187.95</span>
							<input type="text" class="item_quantity" value="1" /><a href="${pageContext.request.contextPath}/cart.jsp">
							<input type="button" class="item_add items" value="添加购物车">
						</a>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
			<div class="product-grid love-grid">
					<a href="${pageContext.request.contextPath}/single.jsp">
					<div class="more-product"><span> </span></div>
					<div class="product-img b-link-stripe b-animate-go  thickbox">
						<img src="${pageContext.request.contextPath}/assets/men/men3.jpg" class="img-responsive" alt=""/>
						<div class="b-wrapper">
							<h4 class="b-animate b-from-left  b-delay03">
								<button class="btns">查看商品详情</button>
							</h4>
						</div>
					</div>
					</a>
				<div class="product-info simpleCart_shelfItem">
					<div class="product-info-cust">
						<h4 class="love-info">衣服</h4>
						<span class="item_price">单价::￥187.95</span>
						<input type="text" class="item_quantity" value="1" />
						<a href="${pageContext.request.contextPath}/cart.jsp">
							<input type="button" class="item_add items" value="添加购物车">
						</a>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
		<div class="rsidebar span_1_of_left">
			<section  class="sky-form">
				<div class="product_right">
					<h3 class="m_2">类别</h3>
					<div class="tab1">
						<ul class="place">
							<li class="sort">牛仔类</li>
							<li class="by"><img src="${pageContext.request.contextPath}/assets/images/images/do.png" alt=""></li>
							<div class="clearfix"> </div>
						</ul>
						<div class="single-bottom">
							<a href="#"><p>牛仔衣</p></a>
							<a href="#"><p>牛仔裤</p></a>
							<a href="#"><p>牛仔裙</p></a>
							<a href="#"><p>牛仔帽</p></a>
						</div>
					</div>
					<div class="tab2">
						<ul class="place">
							<li class="sort">衬衫</li>
							<li class="by"><img src="${pageContext.request.contextPath}/assets/images/images/do.png" alt=""></li>
							<div class="clearfix"> </div>
						</ul>
						<div class="single-bottom">
							<a href="#"><p>白色领——品味型</p></a>
							<a href="#"><p>敞角领——浪漫型</p></a>
							<a href="#"><p>纽扣领一一运动型</p></a>
							<a href="#"><p>长尖领——时尚型</p></a>
						</div>
					</div>
					<div class="tab3">
						<ul class="place">
							<li class="sort">西服</li>
							<li class="by"><img src="${pageContext.request.contextPath}/assets/images/images/do.png" alt=""></li>
							<div class="clearfix"> </div>
						</ul>
						<div class="single-bottom">
							<a href="#"><p>礼服</p></a>
							<a href="#"><p>便服</p></a>
						</div>
					</div>
					<div class="tab4">
						<ul class="place">
							<li class="sort">休闲</li>
							<li class="by"><img src="${pageContext.request.contextPath}/assets/images/images/do.png" alt=""></li>
							<div class="clearfix"> </div>
						</ul>
						<div class="single-bottom">
							<a href="#"><p>前卫休闲</p></a>
							<a href="#"><p>运动休闲</p></a>
							<a href="#"><p>民俗休闲</p></a>
							<a href="#"><p>古典休闲</p></a>
						</div>
					</div>
					<div class="tab5">
						<ul class="place">
							<li class="sort">裤子</li>
							<li class="by"><img src="${pageContext.request.contextPath}/assets/images/images/do.png" alt=""></li>
							<div class="clearfix"> </div>
						</ul>
						<div class="single-bottom">
							<a href="#"><p>长裤</p></a>
							<a href="#"><p>马裤</p></a>
						</div>
					</div>
						  
						  <!--script-->
					<script>
						$(document).ready(function(){
							$(".tab1 .single-bottom").hide();
							$(".tab2 .single-bottom").hide();
							$(".tab3 .single-bottom").hide();
							$(".tab4 .single-bottom").hide();
							$(".tab5 .single-bottom").hide();
								
							$(".tab1 ul").click(function(){
								$(".tab1 .single-bottom").slideToggle(300);
								$(".tab2 .single-bottom").hide();
								$(".tab3 .single-bottom").hide();
								$(".tab4 .single-bottom").hide();
								$(".tab5 .single-bottom").hide();
							})
							$(".tab2 ul").click(function(){
								$(".tab2 .single-bottom").slideToggle(300);
								$(".tab1 .single-bottom").hide();
								$(".tab3 .single-bottom").hide();
								$(".tab4 .single-bottom").hide();
								$(".tab5 .single-bottom").hide();
							})
							$(".tab3 ul").click(function(){
								$(".tab3 .single-bottom").slideToggle(300);
								$(".tab4 .single-bottom").hide();
								$(".tab5 .single-bottom").hide();
								$(".tab2 .single-bottom").hide();
								$(".tab1 .single-bottom").hide();
							})
							$(".tab4 ul").click(function(){
								$(".tab4 .single-bottom").slideToggle(300);
								$(".tab5 .single-bottom").hide();
								$(".tab3 .single-bottom").hide();
								$(".tab2 .single-bottom").hide();
								$(".tab1 .single-bottom").hide();
							})
							$(".tab5 ul").click(function(){
								$(".tab5 .single-bottom").slideToggle(300);
								$(".tab4 .single-bottom").hide();
								$(".tab3 .single-bottom").hide();
								$(".tab2 .single-bottom").hide();
								$(".tab1 .single-bottom").hide();
							})
						});
					</script>
					<!-- script -->
				<section  class="sky-form">
					<h4>优惠</h4>
					<div class="row row1 scroll-pane">
						<div class="col col-4">
							<label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>最高 - 10% (20)</label>
						</div>
						<div class="col col-4">
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>40% - 50% (5)</label>
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>30% - 20% (7)</label>
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>10% - 5% (2)</label>
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>其他(50)</label>
						</div>
					</div>
				</section>
				<section  class="sky-form">
					<h4>品牌</h4>
					<div class="row row1 scroll-pane">
						<div class="col col-4">
							<label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>Roadstar</label>
						</div>
						<div class="col col-4">
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>VAPH</label>
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Puma</label>
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Oakley</label>
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Manga</label>
							<label class="checkbox"><input type="checkbox" name="checkbox" ><i></i>Pepe Jeans</label>
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Crocodile</label>
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Zumba</label>
						</div>
					</div>
				</section>
				<section  class="sky-form">
					<h4>价格</h4>
					<div class="row row1 scroll-pane">
						<div class="col col-4">
							<label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>$50.00 以下</label>
						</div>
						<div class="col col-4">
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>$100.00 以下</label>
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>$200.00 以下</label>
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>$300.00 以下</label>
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>$400.00 以下</label>
						</div>
					</div>
				</section>
				</div>
			 <div class="clearfix"></div>
			</section>
		</div>
</div>
</div>

<jsp:include page="footer.jsp"/>