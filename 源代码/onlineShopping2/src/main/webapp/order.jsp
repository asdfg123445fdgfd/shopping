<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="renderer" content="webkit">
		<meta name="applicable-device" content="pc,mobile">
	<title>我的订单</title>
	<link href="${pageContext.request.contextPath}/assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/allstyle.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/menu.css" rel="stylesheet" type="text/css" media="all" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/css/component.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/styles.css">

	<script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/simpleCart.min.js"> </script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/menu.js"></script>
<!-- start menu -->

<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>

	<!-- start menu -->
</head>
<body>
<jsp:include page="header.jsp"/>
<div class="blog">
	 <div class="container">
		 <ol class="breadcrumb">
		   <li><a href="${pageContext.request.contextPath}/index.jsp">主页</a></li>
			 <li><a href="${pageContext.request.contextPath}/mycenter.jsp">个人中心</a></li>
			 <li class="active">我的订单</li>
		 </ol>
	 <h2>我的订单</h2>
         <div class="col-md-9 fashion-blogs">
             <div class="comments-main" style="height: 550px">
				 <div class="order1" id="navLt">
					 <span class="left_yh font24 width20 tcenter cursor onHover onorange slect">全部订单(50)</span>
					 <span class="left_yh font24 width20 tcenter cursor onHover onorange">待付款(2)</span>
					 <span class="left_yh font24 width20 tcenter cursor onHover onorange">待发货(5)</span>
					 <span class="left_yh font24 width20 tcenter cursor onHover onorange">待收货(2)</span>
					 <span class="left_yh font24 width20 tcenter cursor onHover onorange">待评价(2)</span>
				 </div>
				 <div class="allGoods width100 hidden_yh hhD" style="display: block;">
					 <!--bottom(一条)-->
					 <div class="width100 hidden_yh">
						 <!--标题-->
						 <div class="width100 hidden_yh font20 c_66" style="background:#faf5f5;text-indent:47px;height:50px;line-height:50px;border-bottom:1px solid #ddd;">
							 2017.09.28 14:30
							 订单号600123456001
							 物流运输中
						 </div>
						 <!--之一-->
						 <div style="width: 835px;border-bottom:1px dashed #ddd;padding-top:10px;padding-bottom:10px;" class="hidden_yh center_yh">
							 <img src="${pageContext.request.contextPath}/assets/images/images/ab4.jpg" width="100" height="100" class="left_yh">
							 <div class="left_yh" style="width:580px;">
								 <h3 class="font18 c_33 font100">魅力无敌的衣服</h3>
								 <p class="c_66 font16">订单号::50564513215</p>
								 <p class="c_66 font16">尺码::S</p>
								 <p class="red">单价::￥268.00</p>
							 </div>
							 <div class="right_yh">
								 <a href="javascript:void(0)" class="off block_yh tcenter font16 onHoverr" style="margin-top:10px;padding:6px;">订单详情</a>
								 <a href="javascript:void(0)" class="off block_yh tcenter font16 onHoverr" style="margin-top:10px;padding:6px;">退款/退货</a>
								 <a href="javascript:void(0)" class="off block_yh tcenter font16 onHoverr" style="margin-top:10px;padding:6px;">查看物流</a>
							 </div>
						 </div>
						 <!--之一-->
						 <div style="width:838px;border-bottom:1px dashed #ddd;padding-top:10px;padding-bottom:10px;" class="hidden_yh center_yh">
							 <img src="${pageContext.request.contextPath}/assets/images/images/ab4.jpg" width="100" height="100" class="left_yh">
							 <div class="left_yh" style="width:580px;">
								 <h3 class="font18 c_33 font100">好看美丽的衣服你想买嘛？</h3>
								 <p class="order2">订单号::50564513215</p>
								 <p class="order2">尺码::S</p>
								 <p class="order2">单价::￥268.00</p>
							 </div>
							 <div class="right_yh">
								 <a href="javascript:void(0)" class="off block_yh tcenter font16 onHoverr" style="margin-top:10px;padding:6px;">订单详情</a>
								 <a href="javascript:void(0)" class="off block_yh tcenter font16 onHoverr" style="margin-top:10px;padding:6px;">退款/退货</a>
								 <a href="javascript:void(0)" class="off block_yh tcenter font16 onHoverr" style="margin-top:10px;padding:6px;">查看物流</a>
							 </div>
						 </div>
						 <!--总结-->
						 <div style="width:838px;padding-top:15px;padding-bottom:15px;" class="hidden_yh center_yh tleft">
							 <font class="font24" style="margin-left: 10px">总金额<font class="font18 c_66">(含运费0元)：</font></font><font class="red font34"><font class="font24">￥</font>268.00</font>
							 <a href="javascript:void(0)" class="c_33 onHover font20 onorange right_yh">确认收货</a>
						 </div>
						 <ul class="pagination pull-right" style="margin-top: 0px;">
							 <li><a href="#">&laquo;</a></li>
							 <li><a href="#">1</a></li>
							 <li><a href="#">2</a></li>
							 <li><a href="#">3</a></li>
							 <li><a href="#">4</a></li>
							 <li><a href="#">5</a></li>
							 <li><a href="#">&raquo;</a></li>
						 </ul>
					 </div>

				 </div>
             </div>
         </div>
	 <div class="col-md-3 sidebar">
		 <h3>个人资料</h3>
		 <ul>
			 <li><a href="${pageContext.request.contextPath}/mycenter.jsp"><span> </span>基本资料</a></li>
			 <li><a href="#"><span> </span>收货地址</a></li>
			 <li><a href="#"><span> </span>其他</a></li>
		 </ul>

		 <h3 class="arch">安全设置</h3>
		 <ul>
			 <li><a href="${pageContext.request.contextPath}/Chpassword.jsp"><span> </span>修改登录密码</a></li>
			 <li><a href="${pageContext.request.contextPath}/collect.jsp"><span> </span>商品收藏</a></li>
			 <li><a href="${pageContext.request.contextPath}/order.jsp"><span> </span>我的订单</a></li>
			 <li><a href="#"><span> </span>账号申诉</a></li>
		 </ul>
		<div class="clear_fix"></div>

     </div>
	 <div class="clear_fix"> </div>
</div>
</div>
<script>
    $(".mG").click(function(){
        $(this).parent().parent().remove()
    })
    $("#navLt span").click(function(){
        var t=$(this).index();
        $(this).addClass("slect").siblings().removeClass("slect")
        $(".hhD").eq(t).css({display:"block"}).siblings(".hhD").css({display:"none"});
    })
</script>
<jsp:include page="footer.jsp"/>