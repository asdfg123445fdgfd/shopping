<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="renderer" content="webkit">
		<meta name="applicable-device" content="pc,mobile">
	<title>recommend</title>
	<link href="${pageContext.request.contextPath}/assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/allstyle.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/component.css" rel="stylesheet" type="text/css"  />
	<link href="${pageContext.request.contextPath}/assets/css/menu.css" rel="stylesheet" type="text/css" media="all" />
	<script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/simpleCart.min.js"> </script>
	<script src="${pageContext.request.contextPath}/assets/js/menu.js" type="text/javascript" ></script>
<!-- start menu -->


<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
<!-- start menu -->
</head>
<body>
<jsp:include page="header.jsp"/>
<div class="shop">
       <div class="container">
	   <ol class="breadcrumb">

		  <li><a href="${pageContext.request.contextPath}/index.jsp">主页</a></li>

		  <li class="active">今日推荐</li>
		 </ol>
		 <div class="roses-head">	
			  <h2>NEW STYLES</h2>
		  </div>
      </div>
</div>
<!---->
<div class="features" id="features">
	 <div class="container">
		 <div class="tabs-box">
			<ul class="tabs-menu">
				<li class="active" style="display: inline"><a href="#tab1">今日推荐</a></li>
				<li><a href="#">欧美</a></li>
				<li><a href="#">韩范</a></li>
			</ul>
		 </div>
		 <div class="clear_fix"> </div>
		 <div class="tab-grids">
			 <div id="tab1" class="tab-grid1">
				 <div class="product-grid boats-grid">

						<a href="${pageContext.request.contextPath}/single.jsp">


							<div class="product-img b-link-stripe b-animate-go  thickbox">
								<img src="${pageContext.request.contextPath}/assets/women/women19.jpg" class="img-responsive" alt=""/>
								<div class="b-wrapper">
									<h4 class="b-animate b-from-left  b-delay03">
										<button class="btns">查看商品详情</button>
									</h4>
								</div>
							</div>
						</a>
						<div class="product-info simpleCart_shelfItem">
							<div class="product-info-cust">
								<h4>商品名称</h4>
								<span class="item_price">单价::￥187.95</span>
								<input type="text" class="item_quantity" value="1" />

								<a href="${pageContext.request.contextPath}/cart.jsp">

									<input type="button" class="item_add" value="添加购物车">
								</a>
							</div>														
							<div class="clear_fix"> </div>
						</div>
				 </div>
				 <div class="product-grid boats-grid">

					 <a href="${pageContext.request.contextPath}/single.jsp">


						 <div class="product-img b-link-stripe b-animate-go  thickbox">
							 <img src="${pageContext.request.contextPath}/assets/men/men9.jpg" class="img-responsive" alt=""/>
							 <div class="b-wrapper">
								 <h4 class="b-animate b-from-left  b-delay03">
									 <button class="btns">查看商品详情</button>
								 </h4>
							 </div>
						 </div>
					 </a>
						<div class="product-info simpleCart_shelfItem">
							<div class="product-info-cust">
								<h4>商品名称</h4>
								<span class="item_price">单价::￥187.95</span>
								<input type="text" class="item_quantity" value="1" />

								<a href="${pageContext.request.contextPath}/cart.jsp">

									<input type="button" class="item_add" value="添加购物车">
								</a>
							</div>													
							<div class="clear_fix"> </div>
						</div>
				 </div>
				 <div class="product-grid boats-grid">

					 <a href="${pageContext.request.contextPath}/single.jsp">


						 <div class="product-img b-link-stripe b-animate-go  thickbox">
						 	<img src="${pageContext.request.contextPath}/assets/men/men1.jpg" class="img-responsive" alt=""/>
							 <div class="b-wrapper">
							 	<h4 class="b-animate b-from-left  b-delay03">
									 <button class="btns">查看商品详情</button>
							 	</h4>
						 	 </div>
						 </div>
					 </a>
						<div class="product-info simpleCart_shelfItem">
							<div class="product-info-cust">
								<h4>啦啦啦啦</h4>
								<span class="item_price">单价::￥187.95</span>
								<input type="text" class="item_quantity" value="1" />

								<a href="${pageContext.request.contextPath}/cart.jsp">

									<input type="button" class="item_add" value="添加购物车">
								</a>
							</div>													
							<div class="clear_fix"> </div>
						</div>
					</div>
				 <div class="product-grid boats-grid">

					 <a href="${pageContext.request.contextPath}/single.jsp">


						<div class="product-img b-link-stripe b-animate-go  thickbox">
							<img src="${pageContext.request.contextPath}/assets/women/women9.jpg" class="img-responsive" alt=""/>
							<div class="b-wrapper">
							<h4 class="b-animate b-from-left  b-delay03">							
							<button class="btns">查看商品详情</button>
							</h4>
							</div>
						</div>
					 </a>
						<div class="product-info simpleCart_shelfItem">
							<div class="product-info-cust">
								<h4>商品的名称</h4>
								<span class="item_price">单价::￥187.95</span>
								<input type="text" class="item_quantity" value="1" />

								<a href="${pageContext.request.contextPath}/cart.jsp">

								<input type="button" class="item_add" value="添加购物车">
								</a>
							</div>
							<div class="clear_fix"> </div>
						</div>
				 </div>
				 <div class="product-grid boats-grid">

					 <a href="${pageContext.request.contextPath}/single.jsp">


						<div class="product-img b-link-stripe b-animate-go  thickbox">
							<img src="${pageContext.request.contextPath}/assets/women/women11.jpg" class="img-responsive" alt=""/>
							<div class="b-wrapper">
							<h4 class="b-animate b-from-left  b-delay03">							
							<button class="btns">查看商品详情</button>
							</h4>
							</div>
						</div>
					 </a>
						<div class="product-info simpleCart_shelfItem">
							<div class="product-info-cust">
								<h4>商品的名称</h4>
								<span class="item_price">单价::￥187.95</span>
								<input type="text" class="item_quantity" value="1" />

								<a href="${pageContext.request.contextPath}/cart.jsp">

									<input type="button" class="item_add" value="添加购物车">
								</a>
							</div>													
							<div class="clear_fix"> </div>
						</div>
				 </div>
				 <div class="product-grid boats-grid">

					 <a href="${pageContext.request.contextPath}/single.jsp">


						<div class="product-img b-link-stripe b-animate-go  thickbox">
							<img src="${pageContext.request.contextPath}/assets/men/men10.jpg" class="img-responsive" alt=""/>
							<div class="b-wrapper">
							<h4 class="b-animate b-from-left  b-delay03">							
							<button class="btns">查看商品详情</button>
							</h4>
							</div>
						</div>
					 </a>
						<div class="product-info simpleCart_shelfItem">
							<div class="product-info-cust">
								<h4>商品的名称</h4>
								<span class="item_price">单价::￥187.95</span>
								<input type="text" class="item_quantity" value="1" />
									<a href="${pageContext.request.contextPath}/cart.jsp">

									<input type="button" class="item_add" value="添加购物车">
								</a>
							</div>													
							<div class="clear_fix"> </div>
						</div>
				 </div>
					<div class="clear_fix"></div>
			 </div>

			 

			 <div id="tab2" class="tab-grid2">
				 <div class="product-grid boats-grid">
					 <a href="${pageContext.request.contextPath}/single.jsp">

						<div class="product-img b-link-stripe b-animate-go  thickbox">
							<img src="${pageContext.request.contextPath}/assets/men/men8.jpg" class="img-responsive" alt=""/>
							<div class="b-wrapper">
							<h4 class="b-animate b-from-left  b-delay03">							
							<button class="btns">查看商品详情</button>
							</h4>
							</div>
						</div>
					 </a>
						<div class="product-info simpleCart_shelfItem">
							<div class="product-info-cust">
								<h4>商品的详情</h4>
								<span class="item_price">单价::￥187.95</span>
								<input type="text" class="item_quantity" value="1" />
								<a href="${pageContext.request.contextPath}/cart.jsp">
									<input type="button" class="item_add" value="添加购物车">
								</a>
							</div>							
													
							<div class="clear_fix"> </div>
						</div>
				 </div>
				 <div class="product-grid boats-grid">
					 <a href="${pageContext.request.contextPath}/single.jsp">

						<div class="product-img b-link-stripe b-animate-go  thickbox">
							<img src="${pageContext.request.contextPath}/assets/images/images/c2.jpg" class="img-responsive" alt=""/>
							<div class="b-wrapper">
							<h4 class="b-animate b-from-left  b-delay03">							
							<button class="btns">查看商品详情</button>
							</h4>
							</div>
						</div>
					 </a>
						<div class="product-info simpleCart_shelfItem">
							<div class="product-info-cust">
								<h4>飒飒飒飒是所所所</h4>
								<span class="item_price">单价::￥187.95</span>
								<input type="text" class="item_quantity" value="1" />
								<a href="${pageContext.request.contextPath}/cart.jsp">
									<input type="button" class="item_add" value="添加购物车">
								</a>
							</div>
							<div class="clear_fix"> </div>
						</div>
				 </div>
				 <div class="product-grid boats-grid">
					 <a href="${pageContext.request.contextPath}/single.jsp">

						<div class="product-img b-link-stripe b-animate-go  thickbox">
							<img src="${pageContext.request.contextPath}/assets/images/images/c3.jpg" class="img-responsive" alt=""/>
							<div class="b-wrapper">
							<h4 class="b-animate b-from-left  b-delay03">							
							<button class="btns">查看商品详情</button>
							</h4>
							</div>
						</div>
					 </a>
						<div class="product-info simpleCart_shelfItem">
							<div class="product-info-cust">
								<h4>哒哒哒哒哒哒</h4>
								<span class="item_price">单价::￥187.95</span>
								<input type="text" class="item_quantity" value="1" />
								<a href="${pageContext.request.contextPath}/cart.jsp">
									<input type="button" class="item_add" value="添加购物车">
								</a>
							</div>							
														
							<div class="clear_fix"> </div>
						</div>
					</div>
				 <div class="product-grid boats-grid">
					 <a href="${pageContext.request.contextPath}/single.jsp">

						 <div class="product-img b-link-stripe b-animate-go  thickbox">
							<img src="${pageContext.request.contextPath}/assets/images/images/c4.jpg" class="img-responsive" alt=""/>
							<div class="b-wrapper">
							<h4 class="b-animate b-from-left  b-delay03">
							<button class="btns">查看商品详情</button>
							</h4>
							</div>
						</div>
					 </a>
						<div class="product-info simpleCart_shelfItem">
							<div class="product-info-cust">
								<h4>哒哒哒哒哒哒多多多多大奥奥奥</h4>
								<span class="item_price">单价::￥187.95</span>
								<input type="text" class="item_quantity" value="1" />
								<a href="${pageContext.request.contextPath}/cart.jsp">
									<input type="button" class="item_add" value="添加购物车">
								</a>
							</div>
							<div class="clear_fix"> </div>
						</div>
					</div>
				 <div class="product-grid boats-grid">
					 <a href="${pageContext.request.contextPath}/single.jsp">
						<div class="product-img b-link-stripe b-animate-go  thickbox">
							<img src="${pageContext.request.contextPath}/assets/images/images/c5.jpg" class="img-responsive" alt=""/>
							<div class="b-wrapper">
							<h4 class="b-animate b-from-left  b-delay03">
							<button class="btns">查看商品详情</button>
							</h4>
							</div>
						</div>
					 </a>
						<div class="product-info simpleCart_shelfItem">
							<div class="product-info-cust">
								<h4>恩恩额恩恩</h4>
								<span class="item_price">单价::￥187.95</span>
								<input type="text" class="item_quantity" value="1" />
								<a href="${pageContext.request.contextPath}/cart.jsp">
								<input type="button" class="item_add" value="添加购物车">
								</a>
							</div>
							<div class="clear_fix"> </div>
						</div>
					</div>
				 <div class="product-grid boats-grid">
					 <a href="${pageContext.request.contextPath}/single.jsp">
						<div class="product-img b-link-stripe b-animate-go  thickbox">
							<img src="${pageContext.request.contextPath}/assets/images/images/c6.jpg" class="img-responsive" alt=""/>
							<div class="b-wrapper">
							<h4 class="b-animate b-from-left  b-delay03">
							<button class="btns">查看商品详情</button>
							</h4>
							</div>
						</div>
					 </a>
						<div class="product-info simpleCart_shelfItem">
							<div class="product-info-cust">
								<h4>飒飒飒飒飒飒啊啊啊啊啊啊啊</h4>
								<span class="item_price">单价::￥187.95</span>
								<input type="text" class="item_quantity" value="1" />
								<a href="${pageContext.request.contextPath}/cart.jsp">
									<input type="button" class="item_add" value="添加购物车">
								</a>
							</div>
							<div class="clear_fix"> </div>
						</div>
					</div>
					<div class="clear_fix"></div>

			 </div>
			 <div id="tab3" class="tab-grid3">
				 <div class="product-grid boats-grid">
					 <a href="${pageContext.request.contextPath}/single.jsp">
						 <div class="product-img b-link-stripe b-animate-go  thickbox">
							 <img src="${pageContext.request.contextPath}/assets/images/images/ab1.jpg" class="img-responsive" alt=""/>
							 <div class="b-wrapper">
								 <h4 class="b-animate b-from-left  b-delay03">
									 <button class="btns">查看商品详情</button>
								 </h4>
							 </div>
						 </div>
					 </a>
					 <div class="product-info simpleCart_shelfItem">
						 <div class="product-info-cust">
							 <h4>商品的详情</h4>
							 <span class="item_price">单价::￥187.95</span>
							 <input type="text" class="item_quantity" value="1" />
							 <a href="${pageContext.request.contextPath}/cart.jsp">
								 <input type="button" class="item_add" value="添加购物车">
							 </a>
						 </div>

						 <div class="clear_fix"> </div>
					 </div>
				 </div>
				 <div class="product-grid boats-grid">
					 <a href="${pageContext.request.contextPath}/single.jsp">
						 <div class="product-img b-link-stripe b-animate-go  thickbox">
							 <img src="${pageContext.request.contextPath}/assets/images/images/ab2.jpg" class="img-responsive" alt=""/>
							 <div class="b-wrapper">
								 <h4 class="b-animate b-from-left  b-delay03">
									 <button class="btns">查看商品详情</button>
								 </h4>
							 </div>
						 </div>
					 </a>
					 <div class="product-info simpleCart_shelfItem">
						 <div class="product-info-cust">
							 <h4>飒飒飒飒是所所所</h4>
							 <span class="item_price">单价::￥187.95</span>
							 <input type="text" class="item_quantity" value="1" />
							 <a href="${pageContext.request.contextPath}/cart.jsp">
								 <input type="button" class="item_add" value="添加购物车">
							 </a>
						 </div>
						 <div class="clear_fix"> </div>
					 </div>
				 </div>
				 <div class="product-grid boats-grid">
					 <a href="${pageContext.request.contextPath}/single.jsp">
						 <div class="product-img b-link-stripe b-animate-go  thickbox">
							 <img src="${pageContext.request.contextPath}/assets/images/images/ab3.jpg" class="img-responsive" alt=""/>
							 <div class="b-wrapper">
								 <h4 class="b-animate b-from-left  b-delay03">
									 <button class="btns">查看商品详情</button>
								 </h4>
							 </div>
						 </div>
					 </a>
					 <div class="product-info simpleCart_shelfItem">
						 <div class="product-info-cust">
							 <h4>哒哒哒哒哒哒</h4>
							 <span class="item_price">单价::￥187.95</span>
							 <input type="text" class="item_quantity" value="1" />
							 <a href="${pageContext.request.contextPath}/cart.jsp">
								 <input type="button" class="item_add" value="添加购物车">
							 </a>
						 </div>

						 <div class="clear_fix"> </div>
					 </div>
				 </div>
				 <div class="product-grid boats-grid">
					 <a href="${pageContext.request.contextPath}/single.jsp">
						 <div class="product-img b-link-stripe b-animate-go  thickbox">
							 <img src="${pageContext.request.contextPath}/assets/images/images/ab4.jpg" class="img-responsive" alt=""/>
							 <div class="b-wrapper">
								 <h4 class="b-animate b-from-left  b-delay03">
									 <button class="btns">查看商品详情</button>
								 </h4>
							 </div>
						 </div>
					 </a>
					 <div class="product-info simpleCart_shelfItem">
						 <div class="product-info-cust">
							 <h4>哒哒哒哒哒哒多多多多大奥奥奥</h4>
							 <span class="item_price">单价::￥187.95</span>
							 <input type="text" class="item_quantity" value="1" />
							 <a href="${pageContext.request.contextPath}/cart.jsp">
								 <input type="button" class="item_add" value="添加购物车">
							 </a>
						 </div>
						 <div class="clear_fix"> </div>
					 </div>
				 </div>
				 <div class="product-grid boats-grid">
					 <a href="${pageContext.request.contextPath}/single.jsp">
						 <div class="product-img b-link-stripe b-animate-go  thickbox">
							 <img src="${pageContext.request.contextPath}/assets/images/images/ab5.jpg" class="img-responsive" alt=""/>
							 <div class="b-wrapper">
								 <h4 class="b-animate b-from-left  b-delay03">
									 <button class="btns">查看商品详情</button>
								 </h4>
							 </div>
						 </div>
					 </a>
					 <div class="product-info simpleCart_shelfItem">
						 <div class="product-info-cust">
							 <h4>恩恩额恩恩</h4>
							 <span class="item_price">单价::￥187.95</span>
							 <input type="text" class="item_quantity" value="1" />
							 <a href="${pageContext.request.contextPath}/cart.jsp">
								 <input type="button" class="item_add" value="添加购物车">
							 </a>
						 </div>
						 <div class="clear_fix"> </div>
					 </div>
				 </div>
				 <div class="product-grid boats-grid">
					 <a href="${pageContext.request.contextPath}/single.jsp">
						 <div class="product-img b-link-stripe b-animate-go  thickbox">
							 <img src="${pageContext.request.contextPath}/assets/images/images/c6.jpg" class="img-responsive" alt=""/>
							 <div class="b-wrapper">
								 <h4 class="b-animate b-from-left  b-delay03">
									 <button class="btns">查看商品详情</button>
								 </h4>
							 </div>
						 </div>
					 </a>
					 <div class="product-info simpleCart_shelfItem">
						 <div class="product-info-cust">
							 <h4>飒飒飒飒飒飒啊啊啊啊啊啊啊</h4>
							 <span class="item_price">单价::￥187.95</span>
							 <input type="text" class="item_quantity" value="1" />
							 <a href="${pageContext.request.contextPath}/cart.jsp">
								 <input type="button" class="item_add" value="添加购物车">
							 </a>
						 </div>
						 <div class="clear_fix"> </div>
					 </div>
				 </div>
				 <div class="clear_fix"></div>

			 </div>
		 </div>
			<!-- tabs-box -->
			<!-- Comman-js-files -->
			<script>
			$(document).ready(function() {
				$("#tab2").hide();
				$("#tab3").hide();
				$(".tabs-menu a").click(function(event){
					event.preventDefault();
					var tab=$(this).attr("href");
					$(".tab-grid1,.tab-grid2,.tab-grid3").not(tab).css("display","none");
					$(tab).fadeIn("slow");
				});
				$("ul.tabs-menu li a").click(function(){
					$(this).parent().addClass("active a");
					$(this).parent().siblings().removeClass("active a");
				});
			});
			</script>
			<!-- Comman-js-files -->
</div>
</div>
<jsp:include page="footer.jsp"/>