<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="renderer" content="webkit">
		<meta name="applicable-device" content="pc,mobile">
	<title>注册</title>
	<link href="${pageContext.request.contextPath}/assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/allstyle.css" rel="stylesheet" type="text/css" media="all" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/css/component.css" />
	<link href="${pageContext.request.contextPath}/assets/css/menu.css" rel="stylesheet" type="text/css" media="all" />

	<script src="${pageContext.request.contextPath}/assets/js/jquery.easydropdown.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/simpleCart.min.js"> </script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/menu.js"></script>
<!-- start menu -->
<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
<!-- start menu -->
</head>
<body>
<jsp:include page="header.jsp"/>
<div class="registration-form">
	 <div class="container">
		 <ol class="breadcrumb">
			 <li><a href="${pageContext.request.contextPath}/index.jsp">首页</a></li>
			 <li class="active1"><a href="${pageContext.request.contextPath}/admin.jsp">管理员登录</a></li>
			 <li class="active">注册</li>
		 </ol>
		 <h2>Registration</h2>
		 <div class="col-md-6 reg-form">
			 <div class="reg">
				 <p>欢迎，请输入以下内容继续...</p>
				 <p>如果你之前在我们公司注册过，<a href="${pageContext.request.contextPath}/login.jsp">点击这里</a></p>
				 <form action="${pageContext.request.contextPath}/register.do" method="post" class="form-horizontal" role="form">
					 <ul>
						 <li class="text-info">用户名称: </li>
						 <li><input type="text" value="" name="username" placeholder="不得小于六位数" required></li>
					 </ul>
					 <ul>
						 <li class="text-info">用户账号: </li>
						 <li><input type="text" value="" name="usercode" placeholder="不得小于六位数" required></li>
					 </ul>				 
					<ul>
						 <li class="text-info">用户E-mail: </li>
						 <li><input type="text" value="" name="Email" placeholder="不得小于六位数" required></li>
					 </ul>
					 <ul>
						 <li class="text-info">用户密码: </li>
						 <li><input type="password" value="" name="password" placeholder="不得小于六位数" required></li>
					 </ul>
					 <ul>
						 <li class="text-info">确认密码:</li>
						 <li><input type="password" value="" name="repassword" placeholder="不得小于六位数" required></li>
					 </ul>
					 <ul>
						 <li class="text-info">手机号码:</li>
						 <li><input type="text" value="" name="telephone" placeholder="不得小于六位数" required></li>
					 </ul>
					 <ul>
						 <li class="text-info">用户类型:</li>
						 <li><input type="text" value="" name="type" placeholder="1：管理员/0：用户" required></li>
					 </ul>							
					 <input type="submit" value="Register Now">
					 <p class="click">点击这个按钮，你就会同意<a href="#" style="color: #2aacc8">网上商城协议</a> </p>
				 </form>
			 </div>
		 </div>
		 <div class="col-md-6 reg-right">
			 <h3>优美句子</h3>
			 <p style="text-indent:1cm;" >真正有气质的淑女,从不炫耀她所拥有的一切,她不告诉人她读过什么书,
				 去过什么地方,有多少件衣服,买过什么珠宝,因为她没有自卑感。</p>
			 <h3 class="lorem">优美段落</h3>
			 <p style="text-indent:1cm;">姑娘,别委屈自己了,把影响情绪的事都踢得远远的,没事就逛逛夜市买几件漂亮衣服,
				 找几个男的女的去KTV一疯一天,闲了就 ,搞点小破坏,累了就带上耳机睡个舒服觉,
				 高兴就打扮打扮自己,难看就看搞笑视屏在不高兴就吼上几声,人生那么短暂老子凭什么委屈自己.</p>
		 </div>
		 <div class="clear_fix"></div>		 
	 </div>
</div>
<jsp:include page="footer.jsp"/>
		