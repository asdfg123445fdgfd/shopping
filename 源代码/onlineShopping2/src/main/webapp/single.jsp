<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="renderer" content="webkit">
		<meta name="applicable-device" content="pc,mobile">
	<title>Single</title>

	<link href="${pageContext.request.contextPath}/assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/allstyle.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/component.css" rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/assets/css/menu.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/assets/css/etalage.css" rel="stylesheet" type="text/css" media="all" />
	<script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/simpleCart.min.js"> </script>
	<script src="${pageContext.request.contextPath}/assets/js/menu.js" type="text/javascript" ></script>
	
	<script src="${pageContext.request.contextPath}/assets/js/jquery.etalage.min.js"></script>

<!-- start menu -->


<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
<!-- start menu -->
</head>
<body>
<jsp:include page="header.jsp"/>
<!--Single Page starts Here-->
<div class="product-main">
	 <div class="container">
		 <ol class="breadcrumb">
		  <li><a href="index.jsp">主页</a></li>
		  <li class="active">商品详情</li>
		 </ol>
		 <div class="ctnt-bar cntnt">
			 <div class="content-bar">
				 <div class="single-page">					 
					 <!--Include the Etalage files-->

					 <!-- Include the Etalage files -->
					 <script>
							jQuery(document).ready(function($){
					
								$('#etalage').etalage({
									thumb_image_width: 300,
									thumb_image_height: 400,
									source_image_width: 700,
									source_image_height: 800,
									show_hint: true,
									click_callback: function(image_anchor, instance_id){
										alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
									}
								});
								// This is for the dropdown list example:
								$('.dropdownlist').change(function(){
									etalage_show( $(this).find('option:selected').attr('class') );
								});
					
							});
						</script>
						<!--//details-product-slider-->
					 <div class="details-left-slider">
						  <ul id="etalage">
							 <li>

								<a href="optionallink.html">
								<img class="etalage_thumb_image" src="${pageContext.request.contextPath}/assets/images/images/s1.jpg" />
								<img class="etalage_source_image" src="${pageContext.request.contextPath}/assets/images/images/s1.jpg" />
								</a>
							 </li>
							 <li>
								<img class="etalage_thumb_image" src="${pageContext.request.contextPath}/assets/images/images/s2.jpg" />
								<img class="etalage_source_image" src="${pageContext.request.contextPath}/assets/images/images/s2.jpg"/>
							 </li>
							 <li>
								<img class="etalage_thumb_image" src="${pageContext.request.contextPath}/assets/images/images/s3.jpg" />
								<img class="etalage_source_image" src="${pageContext.request.contextPath}/assets/images/s3.jpg" />
							 </li>
							 <li>
								<img class="etalage_thumb_image" src="${pageContext.request.contextPath}/assets/images/images/s4.jpg" />
								<img class="etalage_source_image" src="${pageContext.request.contextPath}/assets/images/images/s4.jpg" />

							 </li>
							 <div class="clearfix"></div>
						 </ul>
					 </div>
					 <div class="details-left-info">
							<h3>${shopv.trade_name}</h3>
								<h4>商品介绍</h4>
							<p>${shopv.trade_price} </p>
							<p class="qty">数量 ::</p><input min="1" type="number" id="quantity" name="quantity" value="1" class="form-control input-small">
						 <p class="qty">库存 ::${shopv.trade_num}</p>
						 <h5>尺码 ::<select style="margin-left: 5px">
							 <option value="volvo">S</option>
							 <option value="saab">M</option>
							 <option value="opel">L</option>
						 </select></h5>

							<div class="flower-type">
							<p>商品编号  ::${shopv.serial_num}</p>
							<p>品牌  ::<a href="#">${shopv.trade_brand}</a></p>
							</div>
						 <div class="btn_form">
							 <a href="${pageContext.request.contextPath}/shop/Look.do?id=${shopv.id}">添加购物车</a>
						 </div>
							<h5>描述  ::</h5>
							<p class="desc">顺应潮流的褐色秀发佩戴着粉色的发夹，两侧的秀发散发出迷人高贵气息，可爱不失高贵，爽朗大方。嫩绿短袖衬衫带来青春洋溢的运动心情。前襟黑色蝴蝶结既时尚又倾心。金黄色背带裙非常惹眼，明快鲜艳的颜色时时刻刻引人注目...</p>
					 </div>
					 <div class="clearfix"></div>				 	
				 </div>
			 </div>
		 </div>		 
		 <div class="clearfix"></div>
		 <div class="single-bottom2">
			 <h6>相关推荐</h6>
				<div class="rltd-posts">
					 <div class="col-md-3 pst1">
<<<<<<< HEAD

=======
>>>>>>> 3a5dadb6767b2b5f764aa14f4e52c1c59e701cb8
						 <img src="${pageContext.request.contextPath}/assets/women/women22.jpg" alt=""/>
						 <h4><a href="products.html">NEWLOOK</a></h4>
						 <a class="pst-crt" href="single.html">商品详情</a>
					 </div>
					 <div class="col-md-3 pst1">
						 <img src="${pageContext.request.contextPath}/assets/women/women9.jpg" alt=""/>
						 <h4><a href="products.html">NEWLOOK</a></h4>
						 <a class="pst-crt" href="single.html">商品详情</a>
					 </div>
					 <div class="col-md-3 pst1">
						 <img src="${pageContext.request.contextPath}/assets/women/women4.jpg" alt=""/>
						 <h4><a href="products.html">SAREES</a></h4>
						 <a class="pst-crt" href="single.html">商品详情</a>
					 </div>
					 <div class="col-md-3 pst1">
						 <img src="${pageContext.request.contextPath}/assets/women/women17.jpg" alt=""/>
						 <h4><a href="products.html">MANGO</a></h4>
						 <a class="pst-crt" href="single.html">商品详情</a>
<<<<<<< HEAD

=======
>>>>>>> 3a5dadb6767b2b5f764aa14f4e52c1c59e701cb8
					 </div>
					 <div class="clearfix"></div>
				</div>
		 </div>	
	 </div>
</div>
<jsp:include page="footer.jsp"/>